﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.BGEngine.Core.Classes;

namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class LuceneIndexerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var loadedRTMPushClient = MockCacheOptions;
            var loadedRemoteLogger = MockRemoteLogger;
            var loadedTenantDataCache = MockTenantDataCache;
        }

        private CacheOptions _MockCacheOptions;
        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;
        
        internal CacheOptions MockCacheOptions
        {
            get
            {
                if (_MockCacheOptions == null)
                {
                    _MockCacheOptions = new CacheOptions();
                }

                return _MockCacheOptions;
            }
        }

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }
    }
}
