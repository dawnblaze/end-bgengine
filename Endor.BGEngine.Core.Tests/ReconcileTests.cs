﻿using Endor.BGEngine.Core.Web.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class ReconcileTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var loadedRemoteLogger = MockRemoteLogger;
            var loadedTenantDataCache = MockTenantDataCache;
        }

        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }

        [TestMethod]
        public async Task Reconcile()
        {
            var ctx = new ApiContext(MockTenantDataCache, 1);
            var tasker = new ReconciliationTask(MockRemoteLogger, MockTenantDataCache);
            var checkingDate = DateTime.UtcNow;
            await tasker.Reconciliate(1, null, null, DateTime.UtcNow, false, new List<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> { 
               (0, 0, 0, 1)
            }, "", null);
            Assert.IsTrue(ctx.Reconciliation.Any(a => a.CreatedDT > checkingDate));
            ctx.Dispose();
        }
    }
}
