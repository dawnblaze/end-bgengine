﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Endor.BGEngine.Core.Classes;
using Endor.BGEngine.Core.Web.Classes;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class DBBackupTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var loadedRTMPushClient = MockRTMPushClient;
            var loadedRemoteLogger = MockRemoteLogger;
            var loadedTenantDataCache = MockTenantDataCache;
        }

        private IRTMPushClient _MockRTMPushClient;
        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;

        internal IRTMPushClient MockRTMPushClient
        {
            get
            {
                if (_MockRTMPushClient == null)
                {
                    _MockRTMPushClient = new MockRTMPushClient();
                }

                return _MockRTMPushClient;
            }
        }

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }


        [TestMethod]
        public void CreateDBBackup()
        {

            DBBackupTask dbBackupTask = new DBBackupTask(MockTenantDataCache, MockRemoteLogger);
//            await dbBackupTask.CreateDBBackup()
        }


    }
}
