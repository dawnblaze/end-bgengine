﻿using Endor.BGEngine.Core.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Endor.BGEngine.Core.Tests
{
    public class TestStartup : BaseStartup
    {
        public TestStartup(IWebHostEnvironment env) : base(env)
        {
        }
    }
}