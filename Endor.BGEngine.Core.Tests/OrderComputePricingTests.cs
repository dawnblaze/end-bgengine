﻿using Endor.BGEngine.Core.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tenant;
using Hangfire.Server;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class OrderComputePricingTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var loadedRTMPushClient = MockRTMPushClient;
            var loadedRemoteLogger = MockRemoteLogger;
            var loadedTenantDataCache = MockTenantDataCache;
        }

        private IRTMPushClient _MockRTMPushClient;
        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;
        
        internal IRTMPushClient MockRTMPushClient
        {
            get
            {
                if (_MockRTMPushClient == null)
                {
                    _MockRTMPushClient = new MockRTMPushClient();
                }

                return _MockRTMPushClient;
            }
        }

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }

        private readonly int OrderPricingComputeReturnsOrderID = -127;
        private readonly short bid = 1;

        [TestMethod]
        public async Task OrderPricingComputeReturns()
        {

            ApiContext ctx = new ApiContext(MockTenantDataCache, bid);

            //Check for "correct" order and delete if exists
            var orderData = ctx.OrderData.FirstOrDefault(t => t.BID == bid && t.ID == OrderPricingComputeReturnsOrderID);
            if (orderData != null)
            {
                await CleanupOrderPricingComputeReturnsOrderID(ctx, orderData);
            }

            //Create a correct order with EF
            orderData = GetTestOrderData();
            orderData.CompanyID = ctx.SimpleCompanyData.FirstOrDefault(t => t.BID == bid).ID;
            orderData.PickupLocationID = ctx.SimpleLocationData.FirstOrDefault(t => t.BID == bid).ID;
            orderData.TaxGroupID = ctx.SimpleTaxGroup.FirstOrDefault(t => t.BID == bid).ID;
            orderData.ProductionLocationID = orderData.PickupLocationID;
            ctx.OrderData.Add(orderData);
            await ctx.SaveChangesAsync();

            try
            {
                var orderItem = GetTestOrderItemData(orderData);
                var materialDataID = ctx.MaterialData.FirstOrDefault(t => t.BID == bid).ID;
                var component1 = new OrderItemComponent()
                {
                    BID = orderData.BID,
                    OrderID = orderData.ID,
                    ID = -9999,
                    Name = "component1",
                    ComponentID = materialDataID,
                    ComponentType = OrderItemComponentType.Material,
                    TotalQuantity = 1,
                    TotalQuantityOV = false,
                    PriceUnit = 25.00m,
                    PriceUnitOV = true
                };

                orderItem.Components = new List<OrderItemComponent> { component1 };
                ctx.OrderItemData.Add(orderItem);
                await ctx.SaveChangesAsync();

                //Call OrderPricingCompute(params)
                PerformContext performContext = null;
                OrderComputePricing pricingComputer = new OrderComputePricing(MockRTMPushClient, MockRemoteLogger, MockTenantDataCache);
                await pricingComputer.OrderPricingCompute(bid, OrderPricingComputeReturnsOrderID, "low", performContext);

                //Get correct order with EF
                ctx = new ApiContext(MockTenantDataCache, bid);
                orderData = ctx.OrderData.FirstOrDefault(t => t.BID == bid && t.ID == OrderPricingComputeReturnsOrderID);
                orderItem = ctx.OrderItemData.FirstOrDefault(t => t.BID == bid && t.OrderID == orderData.ID);

                Assert.AreEqual(25m, orderItem.PriceComponent);
                Assert.AreEqual(0m, orderItem.PriceSurcharge);
                Assert.AreEqual(25m, orderItem.PriceList);
                Assert.AreEqual(25m, orderItem.PricePreTax);
                Assert.IsNull(orderItem.PriceTax);
                Assert.IsNull(orderItem.PriceTotal);

                Assert.AreEqual(25m, orderData.PriceProductTotal);
                Assert.AreEqual(25m, orderData.PriceNet);
                Assert.AreEqual(25m, orderData.PricePreTax);
                Assert.IsNull(orderData.PriceTax);
                Assert.IsNull(orderData.PriceTotal);


                await pricingComputer.OrderPricingCompute(bid, OrderPricingComputeReturnsOrderID, "low", performContext);

                ctx = new ApiContext(MockTenantDataCache, bid);
                orderData = ctx.OrderData.FirstOrDefault(t => t.BID == bid && t.ID == OrderPricingComputeReturnsOrderID);
                orderItem = ctx.OrderItemData.Include(t => t.Components).FirstOrDefault(t => t.BID == bid && t.OrderID == orderData.ID);

                Assert.AreEqual(25m, orderItem.PriceComponent);
                Assert.AreEqual(0m, orderItem.PriceSurcharge);
                Assert.AreEqual(25m, orderItem.PriceList);
                Assert.AreEqual(25m, orderItem.PricePreTax);
                Assert.IsNull(orderItem.PriceTax);
                Assert.IsNull(orderItem.PriceTotal);

                Assert.AreEqual(25m, orderData.PriceProductTotal);
                Assert.AreEqual(25m, orderData.PriceNet);
                Assert.AreEqual(25m, orderData.PricePreTax);
                Assert.IsNull(orderData.PriceTax);
                Assert.IsNull(orderData.PriceTotal);


                //Change a pricing component values with EF
                orderItem.Components.First().TotalQuantity = 2m;
                orderItem.Components.First().TotalQuantityOV = true;
                orderItem.Components.First().PriceUnit = 5m;
                orderItem.Components.First().PriceUnitOV = true;
                await ctx.SaveChangesAsync();

                await pricingComputer.OrderPricingCompute(bid, OrderPricingComputeReturnsOrderID, "low", performContext);

                ctx = new ApiContext(MockTenantDataCache, bid);
                orderData = ctx.OrderData.FirstOrDefault(t => t.BID == bid && t.ID == OrderPricingComputeReturnsOrderID);
                orderItem = ctx.OrderItemData.FirstOrDefault(t => t.BID == bid && t.OrderID == orderData.ID);

                Assert.AreEqual(10m, orderItem.PriceComponent);
                Assert.AreEqual(0m, orderItem.PriceSurcharge);
                Assert.AreEqual(10m, orderItem.PriceList);
                Assert.AreEqual(10m, orderItem.PricePreTax);
                Assert.IsNull(orderItem.PriceTax);
                Assert.IsNull(orderItem.PriceTotal);

                Assert.AreEqual(10m, orderData.PriceProductTotal);
                Assert.AreEqual(10m, orderData.PriceNet);
                Assert.AreEqual(10m, orderData.PricePreTax);
                Assert.IsNull(orderData.PriceTax);
                Assert.IsNull(orderData.PriceTotal);


                //Change the item price with EF
                orderItem.PriceComponent = 100m; // I needed to set a non-computed field to force the ov price
                orderItem.PriceUnitOV = true;
                await ctx.SaveChangesAsync();

                await pricingComputer.OrderPricingCompute(bid, OrderPricingComputeReturnsOrderID, "low", performContext);

                ctx = new ApiContext(MockTenantDataCache, bid);
                orderData = ctx.OrderData.FirstOrDefault(t => t.BID == bid && t.ID == OrderPricingComputeReturnsOrderID);
                orderItem = ctx.OrderItemData.FirstOrDefault(t => t.BID == bid && t.OrderID == orderData.ID);
                List<OrderItemSurcharge> surcharges = ctx.OrderItemSurcharge.Where(t => t.BID == bid && t.OrderItemID == orderItem.ID).ToList();

                Assert.AreEqual(10m, orderItem.PriceComponent);
                Assert.AreEqual(90m, orderItem.PriceSurcharge);
                Assert.AreEqual(100m, orderItem.PriceList);
                Assert.AreEqual(100m, orderItem.PricePreTax);
                Assert.IsNull(orderItem.PriceTax);
                Assert.IsNull(orderItem.PriceTotal);

                Assert.AreEqual(1, surcharges.Count);
                Assert.AreEqual(1, surcharges[0].SurchargeDefID);

                Assert.AreEqual(100m, orderData.PriceProductTotal);
                Assert.AreEqual(100m, orderData.PriceNet);
                Assert.AreEqual(100m, orderData.PricePreTax);
                Assert.IsNull(orderData.PriceTax);
                Assert.IsNull(orderData.PriceTotal);

                //Revert to not OV of the item price with EF
                orderItem.PriceUnitOV = false;
                await ctx.SaveChangesAsync();

                await pricingComputer.OrderPricingCompute(bid, OrderPricingComputeReturnsOrderID, "low", performContext);

                ctx = new ApiContext(MockTenantDataCache, bid);
                orderData = ctx.OrderData.FirstOrDefault(t => t.BID == bid && t.ID == OrderPricingComputeReturnsOrderID);
                orderItem = ctx.OrderItemData.FirstOrDefault(t => t.BID == bid && t.OrderID == orderData.ID);
                surcharges = ctx.OrderItemSurcharge.Where(t => t.BID == bid && t.OrderItemID == orderItem.ID).ToList();

                Assert.AreEqual(10m, orderItem.PriceComponent);
                Assert.AreEqual(0m, orderItem.PriceSurcharge);
                Assert.AreEqual(10m, orderItem.PriceList);
                Assert.AreEqual(10m, orderItem.PricePreTax);
                Assert.IsNull(orderItem.PriceTax);
                Assert.IsNull(orderItem.PriceTotal);

                Assert.AreEqual(0, surcharges.Count);

                Assert.AreEqual(10m, orderData.PriceProductTotal);
                Assert.AreEqual(10m, orderData.PriceNet);
                Assert.AreEqual(10m, orderData.PricePreTax);
                Assert.IsNull(orderData.PriceTax);
                Assert.IsNull(orderData.PriceTotal);
            }
            finally
            {
                //Delete "correct" order for cleanup
                await CleanupOrderPricingComputeReturnsOrderID(ctx, orderData);
            }
        }

        private static async Task CleanupOrderPricingComputeReturnsOrderID(ApiContext ctx, Models.OrderData orderData)
        {
            OrderItemComponent orderItemComponent = ctx.OrderItemComponent.FirstOrDefault(t => t.BID == orderData.BID && t.OrderID == orderData.ID);
            if (orderItemComponent != null)
            {
                ctx.OrderItemComponent.Remove(orderItemComponent);
            }

            int OrderItemID = ctx.OrderItemData.FirstOrDefault(t => t.BID == orderData.BID && t.OrderID == orderData.ID).ID;

            OrderItemSurcharge orderItemSurcharge = ctx.OrderItemSurcharge.FirstOrDefault(t => t.BID == orderData.BID && t.OrderItemID == OrderItemID);
            if (orderItemSurcharge != null)
            {
                ctx.OrderItemSurcharge.Remove(orderItemSurcharge);
            }

            OrderItemData orderItemData = ctx.OrderItemData.FirstOrDefault(t => t.BID == orderData.BID && t.OrderID == orderData.ID);
            if (orderItemComponent != null)
            {
                ctx.OrderItemData.Remove(orderItemData);
            }
        
            ctx.OrderData.Remove(orderData);
            await ctx.SaveChangesAsync();
        }

        /// <summary>
        /// You will need to set  
        /// CompanyID, PickupLocationID 
        /// TaxGroupID, ProductionLocationID
        /// </summary>
        /// <returns></returns>
        private OrderData GetTestOrderData()
        {
            return new OrderData()
            {
                BID = bid,
                ID = OrderPricingComputeReturnsOrderID,
                ClassTypeID = (int)ClassType.Order,
                ModifiedDT = DateTime.UtcNow,
                LocationID = 1,
                TransactionType = (byte)OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
                TaxGroupID = 0
            };
        }

        private  OrderItemData GetTestOrderItemData(OrderData createdOrder)
        {
            return new OrderItemData()
            {
                BID = createdOrder.BID,
                HasCustomImage = false,
                HasDocuments = false,
                HasProof = false,
                ID = -9999,
                OrderID = createdOrder.ID,
                ItemNumber = 1,
                Quantity = 1,
                Name = "test",
                IsOutsourced = false,
                OrderStatusID = createdOrder.OrderStatusID,
                ItemStatusID = 18,
                ProductionLocationID = createdOrder.ProductionLocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = createdOrder.TaxGroupID,
                TaxGroupOV = false,
                IsTaxExempt = false,
                TransactionType = createdOrder.TransactionType

            };
        }
    }
}
