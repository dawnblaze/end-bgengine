﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Endor.Tenant;
using Newtonsoft.Json;

namespace Endor.BGEngine.Core.Tests
{
    public class MockTenantDataCache : ITenantDataCache
    {
        public async Task<TenantData> Get(short bid)
        {

            if (bid == 1)
                return await Task.FromResult(Mock);
            else
                return null;
        }

        public static Dictionary<string, string> GetLocalSettings()
        {
            /*
                Example test-config.json
                {
                    "BusinessDBConnectionString" ="Data Source=.\\SQLEXPRESS;Initial Catalog=\"Dev.Endor.Business.DB1\";User ID=cyrious;Password=watankahani"
                    "StorageConnectionString"
                } 
            */
            string file = "..\\..\\..\\..\\test-config.json"; //put this inside end-bgengine root folder
            if (!File.Exists(file))
            {
                File.WriteAllText(file, @"{ 
""BusinessDBConnectionString"":""Data Source=.\\SQLEXPRESS;Initial Catalog=\""Dev.Endor.Business.DB1\"";User ID=cyrious;Password=watankahani"",
""StorageConnectionString"":""UseDevelopmentStorage=true"",
""StorageURL"":""http://127.0.0.1:10000/devstoreaccount1/""
}");
            }

            string text = File.ReadAllText(file);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);
            return result;
        }

        public void InvalidateCache()
        {
            
        }

        public void InvalidateCache(short bid)
        {
            
        }

        private static TenantData Mock = new TenantData()
        {
            APIURL = "https://endorapi.localcyriousdevelopment.com:5002/",
            ReportingURL = "",
            MessagingURL = "https://endormessaging.localcyriousdevelopment.com:5004/",
            LoggingURL = "https://endorlog.localcyriousdevelopment.com:5003/",
            BackgroundEngineURL = "https://endorbgengine.localcyriousdevelopment.com:5006/",
            SearchURL = "https://endorsearch.localcyriousdevelopment.com:5005/",
            StorageURL = GetLocalSettings()["StorageURL"],
            StorageConnectionString = GetLocalSettings()["StorageConnectionString"],
            //IndexStorageConnectionString = GetLocalSettings()["StorageConnectionString"],
            BusinessDBConnectionString = GetLocalSettings()["BusinessDBConnectionString"],
            LoggingDBConnectionString = "",
            MessagingDBConnectionString = "",
            SystemReportBConnectionString = "",
            SystemDataDBConnectionString = "",
        };
    }
}