﻿using Endor.BGEngine.Core.Classes;
using Endor.BGEngine.Core.Web.Classes;
using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.EF;
using Endor.GLEngine.Models;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tenant;
using Hangfire.Server;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class RecomputeCBELForMachineTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            Teardown();
            var loadedRTMPushClient = MockRTMPushClient;
            var loadedRemoteLogger = MockRemoteLogger;
            var loadedTenantDataCache = MockTenantDataCache;
            this.NewTestMachineName = DateTime.UtcNow + " TEST MACHINE DATA";
            
        }

        private IRTMPushClient _MockRTMPushClient;
        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;
        private string NewTestMachineName;
        private int testMachineTemplateID;

        internal IRTMPushClient MockRTMPushClient
        {
            get
            {
                if (_MockRTMPushClient == null)
                {
                    _MockRTMPushClient = new MockRTMPushClient();
                }

                return _MockRTMPushClient;
            }
        }

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }

        private readonly short bid = 1;

        [TestCleanup]
        public void Teardown()
        {
            ApiContext ctx = new ApiContext(MockTenantDataCache, bid);
            ctx.MachineProfileTable.RemoveRange(ctx.MachineProfileTable.Where(mp => mp.ID < 0));
            ctx.SaveChanges(); 
            ctx.MachineProfile.RemoveRange(ctx.MachineProfile.Where(mp => mp.ID < 0));
            ctx.SaveChanges(); 
            ctx.MachineInstance.RemoveRange(ctx.MachineInstance.Where(mp => mp.ID < 0));
            ctx.SaveChanges(); 
            ctx.MachineData.RemoveRange(ctx.MachineData.Where(mp => mp.ID < 0));
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task RecomputeCBELForMachine()
        {
            ApiContext ctx = new ApiContext(MockTenantDataCache, bid);
            //Add Model
            MachineData itemTest = await GetNewModel(ctx);
            ctx.MachineData.Add(itemTest);
            ctx.SaveChanges();
            var machineInstance = GetTestMachineInstance(ctx);
            machineInstance.MachineID = itemTest.ID;
            ctx.MachineInstance.Add(machineInstance);
            ctx.SaveChanges();
            var machineProfile = GetTestMachineProfile(ctx);
            machineProfile.MachineID = itemTest.ID;
            
            ctx.MachineProfile.Add(machineProfile);
            ctx.SaveChanges();
            var machineProfileTable = GetTestMachineProfileTable(ctx);
            machineProfileTable.ProfileID = machineProfile.ID;
            ctx.MachineProfileTable.Add(machineProfileTable);
            ctx.SaveChanges();
            PerformContext performContext = null;
            RegenerateCBELForMachineTask recomputeTask = new RegenerateCBELForMachineTask(MockRTMPushClient, MockRemoteLogger, MockTenantDataCache);
            await recomputeTask.RegenerateCBELForMachine(bid, itemTest.ID, performContext);


            (ICBELAssembly assembly, string assemblyCode) = await CBELAssemblyHelper.LoadAssemblyFromStorage(itemTest.ID, ClassType.Machine.ID(), 1, _MockTenantDataCache, _MockRemoteLogger, ctx, this.bid);
            Assert.IsNotNull(assembly);
            Assert.AreEqual(assembly.ClassTypeID, ClassType.Machine.ID());

        }

        private async Task<MachineData> GetNewModel(ApiContext ctx)
        {
           int glAccountID = ctx.GLAccount.FirstOrDefault().ID;

            short taxCodeID = ctx.TaxabilityCodes.FirstOrDefault().ID;
            //var tables = ctx.AssemblyData.FirstOrDefault(e => e.Tables != null && e.AssemblyType == AssemblyType.MachineTemplate);
            this.testMachineTemplateID = (await ctx.AssemblyData.FirstOrDefaultAsync(e => e.AssemblyType == AssemblyType.MachineTemplate))?.ID ?? 0;
            if (this.testMachineTemplateID==0)
            {
                AssemblyData machineTemplate1 = new AssemblyData()
                {
                    BID = this.bid,
                    Name = DateTime.UtcNow + " TEST MACHINE TEMPLATE 1",
                    AssemblyType = AssemblyType.MachineTemplate,
                    MachineLayoutTypes = (MachineLayoutType)3,
                    ID = -99,
                };
                ctx.AssemblyData.Add(machineTemplate1);
                ctx.SaveChanges();
            }
            return new MachineData()
            {
                Name = this.NewTestMachineName,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                TaxCodeID = taxCodeID,
                IsActive = true,
                BID = this.bid,
                MachineTemplateID = this.testMachineTemplateID,
                ActiveInstanceCount = 0,
                ActiveProfileCount = 0,
                Description = "Description",
                HasImage = false,
                SKU = "SKU",
                WorksheetData = "[]",
                ID = -99,
            };
        }

        private MachineInstance GetTestMachineInstance(ApiContext ctx)
        {
            return new MachineInstance
            {
                IsActive = true,
                Name = this.NewTestMachineName + " - Instance",
                LocationID = ctx.LocationData.FirstOrDefault().ID,
                Manufacturer = "Manufacturer",
                SerialNumber = "SerialNumber",
                IPAddress = "IPAddress",
                PurchaseDate = DateTime.Now,
                HasServiceContract = true,
                ContractNumber = "",
                ContractStartDate = DateTime.Now,
                ContractEndDate = DateTime.Now,
                ContractInfo = "ContractInfo",
                Model = "Model",
                BID = this.bid,
                ID=-99
            };
        }

        private MachineProfile GetTestMachineProfile(ApiContext ctx)
        {
            return new MachineProfile
            {
                IsActive = true,
                IsDefault = true,
                Name = this.NewTestMachineName + " - Profile",
                MachineTemplateID = this.testMachineTemplateID,
                AssemblyOVDataJSON = "{}",
                BID = this.bid,
                ID = -99
            };
        }

        private MachineProfileTable GetTestMachineProfileTable(ApiContext ctx)
        {
            return new MachineProfileTable
            {
                BID = this.bid,
                ProfileID = -99,
                ID=-99,
                TableID = ctx.AssemblyTable.FirstOrDefault().ID,
                Label = "Assembly table 1",
                OverrideDefault = false
            };
        }
    }
}
