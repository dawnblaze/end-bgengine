﻿using Endor.BGEngine.Core.Classes;
using Endor.EF;
using Endor.GLEngine.Models;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tenant;
using Hangfire.Server;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class RecomputeGLTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var loadedRTMPushClient = MockRTMPushClient;
            var loadedRemoteLogger = MockRemoteLogger;
            var loadedTenantDataCache = MockTenantDataCache;
        }

        private IRTMPushClient _MockRTMPushClient;
        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;

        internal IRTMPushClient MockRTMPushClient
        {
            get
            {
                if (_MockRTMPushClient == null)
                {
                    _MockRTMPushClient = new MockRTMPushClient();
                }

                return _MockRTMPushClient;
            }
        }

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }

        private readonly int OrderPricingComputeReturnsOrderID = -127;
        private readonly short bid = 1;
        [TestMethod]
        public async Task RecomputeTest()
        {

            ApiContext ctx = new ApiContext(MockTenantDataCache, bid);

            //Check for "correct" order and delete if exists
            var orderData = ctx.OrderData.FirstOrDefault(t => t.BID == bid && t.ID == OrderPricingComputeReturnsOrderID);
            if (orderData != null)
            {
                await CleanupOrderPricingComputeReturnsOrderID(ctx, orderData);
            }

            //Create a correct order with EF
            orderData = GetTestOrderData();
            orderData.CompanyID = ctx.SimpleCompanyData.FirstOrDefault(t => t.BID == bid).ID;
            orderData.PickupLocationID = ctx.SimpleLocationData.FirstOrDefault(t => t.BID == bid).ID;
            orderData.TaxGroupID = ctx.SimpleTaxGroup.FirstOrDefault(t => t.BID == bid).ID;
            orderData.ProductionLocationID = orderData.PickupLocationID;
            ctx.OrderData.Add(orderData);
            await ctx.SaveChangesAsync();
            try
            {
                var orderItem = GetTestOrderItemData(orderData);
                var materialDataID = ctx.MaterialData.FirstOrDefault(t => t.BID == bid).ID;
                var component1 = new OrderItemComponent()
                {
                    BID = orderData.BID,
                    OrderID = orderData.ID,
                    ID = -9999,
                    Name = "component1",
                    ComponentID = materialDataID,
                    ComponentType = OrderItemComponentType.Material,
                    TotalQuantity = 1,
                    TotalQuantityOV = false,
                    PriceUnit = 25.00m,
                    PriceUnitOV = true,
                    IncomeAccountID = ctx.GLAccount.Where(gla => gla.GLAccountType==40).First().ID

                };

                orderItem.Components = new List<OrderItemComponent> { component1 };
                ctx.OrderItemData.Add(orderItem);
                await ctx.SaveChangesAsync();

                PerformContext performContext = null;
                OrderComputePricing pricingComputer = new OrderComputePricing(MockRTMPushClient, MockRemoteLogger, MockTenantDataCache);
                await pricingComputer.OrderPricingCompute(bid, OrderPricingComputeReturnsOrderID, "low", performContext);

                RecomputeGLTask recomputeTask = new RecomputeGLTask(MockRTMPushClient, MockTenantDataCache, MockRemoteLogger);
                await recomputeTask.RecomputeGL(bid, (byte)EnumGLType.Order, OrderPricingComputeReturnsOrderID, "Test", "Test", null, null, (byte)GLEntryType.Order_New, "low", performContext);
                Assert.AreEqual(ctx.ActivityGlactivity.Where(a => a.OrderID == OrderPricingComputeReturnsOrderID).Count(),1);
                Assert.IsTrue(ctx.GLData.Where(gl => gl.OrderID == OrderPricingComputeReturnsOrderID).Count() >= 2);
            }
            finally
            {
                //Delete "correct" order for cleanup
                await CleanupOrderPricingComputeReturnsOrderID(ctx, orderData);
            }
        }

        private static async Task CleanupOrderPricingComputeReturnsOrderID(ApiContext ctx, Models.OrderData orderData)
        {
            var glData = ctx.GLData.Where(gl => gl.OrderID == orderData.ID);
            ctx.GLData.RemoveRange(glData);
            var glActivityData = ctx.ActivityGlactivity.Where(gl => gl.OrderID == orderData.ID);
            ctx.ActivityGlactivity.RemoveRange(glActivityData);
            await ctx.SaveChangesAsync();

            var orderItemComponent = ctx.OrderItemComponent.FirstOrDefault(t => t.BID == orderData.BID && t.OrderID == orderData.ID);
                    if (orderItemComponent != null)
                    {
                        ctx.OrderItemComponent.Remove(orderItemComponent);
                    }

                    var orderItemData = ctx.OrderItemData.FirstOrDefault(t => t.BID == orderData.BID && t.OrderID == orderData.ID);
                    if (orderItemComponent != null)
                    {
                        ctx.OrderItemData.Remove(orderItemData);
                    }

                    ctx.OrderData.Remove(orderData);
                    await ctx.SaveChangesAsync();
                }

                /// <summary>
                /// You will need to set  
                /// CompanyID, PickupLocationID 
                /// TaxGroupID, ProductionLocationID
                /// </summary>
                /// <returns></returns>
                private OrderData GetTestOrderData()
                {
                    return new OrderData()
                    {
                        BID = bid,
                        ID = OrderPricingComputeReturnsOrderID,
                        ClassTypeID = (int)ClassType.Order,
                        ModifiedDT = DateTime.UtcNow,
                        LocationID = 1,
                        TransactionType = (byte)OrderTransactionType.Order,
                        OrderStatusID = OrderOrderStatus.OrderPreWIP,
                        OrderStatusStartDT = DateTime.UtcNow,
                        Number = 1000,
                        FormattedNumber = "INV-1000",
                        PriceTaxRate = 0.01m,
                        PaymentPaid = 0m,
                        TaxGroupID = 0
                    };
                }

                private OrderItemData GetTestOrderItemData(OrderData createdOrder)
                {
                    return new OrderItemData()
                    {
                        BID = createdOrder.BID,
                        HasCustomImage = false,
                        HasDocuments = false,
                        HasProof = false,
                        ID = -9999,
                        OrderID = createdOrder.ID,
                        ItemNumber = 1,
                        Quantity = 1,
                        Name = "test",
                        IsOutsourced = false,
                        OrderStatusID = createdOrder.OrderStatusID,
                        ItemStatusID = 18,
                        ProductionLocationID = createdOrder.ProductionLocationID,
                        PriceIsLocked = false,
                        PriceTaxableOV = false,
                        TaxGroupID = createdOrder.TaxGroupID,
                        TaxGroupOV = false,
                        IsTaxExempt = false,
                        TransactionType = createdOrder.TransactionType

                    };
                }
            }
    }
