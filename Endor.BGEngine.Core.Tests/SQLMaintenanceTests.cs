﻿using Endor.BGEngine.Core.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tenant;
using Hangfire.Server;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class SQLMaintenanceTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var loadedRTMPushClient = MockRTMPushClient;
            var loadedRemoteLogger = MockRemoteLogger;
            var loadedTenantDataCache = MockTenantDataCache;
        }

        private IRTMPushClient _MockRTMPushClient;
        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;
        
        internal IRTMPushClient MockRTMPushClient
        {
            get
            {
                if (_MockRTMPushClient == null)
                {
                    _MockRTMPushClient = new MockRTMPushClient();
                }

                return _MockRTMPushClient;
            }
        }

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }

        private readonly short bid = 1;

        [TestMethod]
        public async Task TestSQLMaintenance()
        {
            SQLMaintenance sMaintenance = new SQLMaintenance(MockTenantDataCache, MockRemoteLogger);
            await sMaintenance.RepairUntrustedConstraint(bid);
        }
        
    }
}
