﻿using System.Threading.Tasks;
using Endor.RTM;
using Endor.RTM.Models;

namespace Endor.BGEngine.Core.Tests
{
    internal class MockRTMPushClient : IRTMPushClient
    {
        public Task SendEmployeeMessage(EmployeeMessage userMsg)
        {
            return Task.FromResult(1);
        }

        public Task SendRefreshMessage(RefreshMessage refreshMsg, string excludedConnectionId = null)
        {
            return Task.FromResult(1);
        }

        public Task SendReportTemplateRefreshMessage(ReportTemplateRefreshMessage refreshMsg)
        {
            return Task.FromResult(1);
        }

        public Task SendSystemMessage(SystemMessage systemMsg)
        {
            return Task.FromResult(1);
        }

        public Task SendUserMessage(UserMessage userMsg)
        {
            return Task.FromResult(1);
        }
    }
}