﻿using Endor.BGEngine.Core.Classes;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tenant;
using Microsoft.AspNetCore.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Endor.EF;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Endor.Models;

namespace Endor.BGEngine.Core.Tests
{
    [TestClass]
    public class DeleteDraftsTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var builder = new WebHostBuilder()
              .UseEnvironment("Development")
              .UseStartup<TestStartup>();
            var testServer = new TestServer(builder);
        }

        private IRTMPushClient _MockRTMPushClient;
        private RemoteLogger _MockRemoteLogger;
        private ITenantDataCache _MockTenantDataCache;

        internal IRTMPushClient MockRTMPushClient
        {
            get
            {
                if (_MockRTMPushClient == null)
                {
                    _MockRTMPushClient = new MockRTMPushClient();
                }

                return _MockRTMPushClient;
            }
        }

        internal RemoteLogger MockRemoteLogger
        {
            get
            {
                if (_MockRemoteLogger == null)
                {
                    _MockRemoteLogger = new RemoteLogger(MockTenantDataCache);
                }

                return _MockRemoteLogger;
            }
        }

        internal ITenantDataCache MockTenantDataCache
        {
            get
            {
                if (_MockTenantDataCache == null)
                {
                    _MockTenantDataCache = new MockTenantDataCache();
                }

                return _MockTenantDataCache;
            }
        }

        private readonly short bid = 1;
        [TestMethod]
        public async Task DeleteExpiredDraftsTest()
        {
            ApiContext ctx = new ApiContext(MockTenantDataCache, bid);

            int UserDraftID;

            if (ctx.UserDraft.Any(x => x.BID == bid))
            {
                UserDraftID = (await ctx.UserDraft.OrderByDescending(x => x.ID).FirstAsync(x => x.BID == bid)).ID;
                UserDraftID += 1;
            }

            else
                UserDraftID = 1000;

            DateTime yesterday = DateTime.Today.AddDays(-2);

            short testUserLinkID = (await ctx.UserLink.FirstOrDefaultAsync(x => x.BID == bid)).ID;

            UserDraft userDraft = new UserDraft()
            {
                BID = bid,
                ID = UserDraftID,
                Description = "Test Draft",
                ExpirationDT = yesterday,
                ObjectCTID = 10200,
                UserLinkID = testUserLinkID
            };
            ctx.UserDraft.Add(userDraft);
            ctx.SaveChanges();

            var userDraftData = await ctx.UserDraft.Where(ud => ud.IsExpired.Value == true).ToListAsync();
            Assert.IsTrue(userDraftData.Count() > 0);

            DeleteDraftsTask deleteDraftsTask = new DeleteDraftsTask(MockTenantDataCache, MockRemoteLogger);
            await deleteDraftsTask.DeleteExpiredDrafts(bid);

            userDraftData = await ctx.UserDraft.Where(ud => ud.IsExpired.Value == true).ToListAsync();
            Assert.AreEqual(userDraftData.Count(), 0);
        }
    }
}
