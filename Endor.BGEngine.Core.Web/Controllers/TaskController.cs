﻿using Endor.Tasks;
using Hangfire.Server;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Controllers
{
    [Authorize(AuthenticationSchemes = "Internal")]
    [Route("task")]
    public class TaskController : Controller
    {
        private readonly HangfireTaskQueuer _queuer;

        public TaskController()
        {
            this._queuer = new HangfireTaskQueuer();
        }

        [HttpGet]
        [Route("index/{bid}/{ctid}")]
        public async Task<IActionResult> IndexMany(short bid, int ctid)
        {
            string result = await this._queuer.IndexClasstype(bid, ctid);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("index/{bid}/{ctid}/multiple/{csvID}")]
        public async Task<IActionResult> IndexMultipleID(short bid, int ctid, string csvID)
        {
            var ids = csvID.Split(',').Select(strID => Convert.ToInt32(strID)).ToList();
            string result = await this._queuer.IndexMultipleID(bid, ctid, ids);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("index/{bid}/{ctid}/{id}")]
        public async Task<IActionResult> IndexSingle(short bid, int ctid, int id)
        {
            string result = await this._queuer.IndexModel(bid, ctid, id);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("test/{bid}")]
        public async Task<IActionResult> Test(short bid)
        {
            string result = await this._queuer.Test(bid);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("emptytrash/{bid}")]
        public async Task<IActionResult> EmptyTrash(short bid)
        {
            string result = await this._queuer.EmptyTrash(bid);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(result);
        }

        [HttpGet]
        [Route("emptytemp/{bid}")]
        public async Task<IActionResult> EmptyTemp(short bid)
        {
            string result = await this._queuer.EmptyTemp(bid);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(result);
        }

        [HttpGet]
        [Route("recomputegl/{bid}/{EnumGLType}/{Id}/{name}/{Subject}/{glEntryType}/{priority}/{CompletedById?}/{CompletedByContactId?}")]
        public async Task<IActionResult> RecomputeGL(short bid, byte EnumGLType, int Id, string name, string Subject, byte glEntryType, string priority, short? CompletedById = null, int? CompletedByContactId = null)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = priority
            };

            string result = await queuer.RecomputeGL(bid, EnumGLType, Id, name, Subject, CompletedById, CompletedByContactId, glEntryType);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("createdbbackup/{bid}/{dbtype}")]
        public async Task<IActionResult> CreateDBBackup(short bid, DbType dptype)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = "low" //we don't have this in the query param
            };

            string result = await queuer.CreateDBBackup(bid, dptype);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("checkorderstatus/{bid}/{orderId}/{status}")]
        public async Task<IActionResult> CheckOrderStatus(short bid, int orderId, int status)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = "medium" //we don't have this in the query param, so default to "medium" as mentioned in the END-1159
            };

            string result = await queuer.CheckOrderStatus(bid, orderId, status);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("OrderPricingCompute/{bid}/{orderID}/{priority}")]
        public async Task<IActionResult> OrderPricingCompute(short bid, int orderID, string priority)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = priority
            };

            string result = await queuer.OrderPricingCompute(bid, orderID);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("OrderPricingComputeTax/{bid}/{orderID}/{priority}")]
        public async Task<IActionResult> OrderPricingComputeTax(short bid, int orderID, string priority)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = priority
            };

            string result = await queuer.OrderPricingComputeTax(bid, orderID);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);
            else
                return Ok(result);
        }

        [HttpGet]
        [Route("DeleteExpiredDrafts/{bid}")]
        public async Task<IActionResult> DeleteExpiredDrafts(short bid)
        {
            string result = await this._queuer.DeleteExpiredDrafts(bid);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(result);
        }

        [HttpGet]
        [Route("RepairUntrustedConstraint/{bid}")]
        public async Task<IActionResult> RepairUntrustedConstraint(short bid)
        {
            string result = await this._queuer.RepairUntrustedConstraint(bid);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(result);
        }

        [HttpGet]
        [Route("RegenerateCBELForMachine/{bid}/{machineID}")]
        public async Task<IActionResult> RegenerateCBELForMachine(short bid, int machineID)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = "medium"
            };

            string result = await queuer.RegenerateCBELForMachine(bid, machineID);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(result);
        }

        [HttpGet]
        [Route("failtest")]
        public IActionResult FailTest()
        {
            Expression<Action> expression = () => FailDoingStuff();
            Hangfire.BackgroundJob.Enqueue(expression);

            return Ok();
        }

        [HttpGet]
        [Route("ClearAllAssemblies/{bid}")]
        public async Task<IActionResult> ClearAllAssemblies(short bid)
        {
            string result = await this._queuer.ClearAllAssemblies(bid);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(result);
        }

        [HttpGet]
        [Route("ClearAssembly/{bid}/{ctid}/{id}")]
        public async Task<IActionResult> ClearAssembly(short bid, int ctid, int id)
        {
            string result = await this._queuer.ClearAssembly(bid, ctid, id);

            if (result == null)
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(result);
        }

        [HttpGet]
        [Route("DocumentReport/{aid}/{bid}/{documentReportType}/menu/{menuID}")]
        public async Task<IActionResult> GenerateDocumentReportByMenuID(
            byte aid,
            short bid,
            byte documentReportType,
            int menuID,
            [FromQuery]string priority,
            [FromQuery]string schedule,
            [FromQuery]int[] DataID = null,
            [FromQuery]int[] CustomFields = null,
            [FromQuery]string OrderStatusFilter = null,
            [FromQuery]string OptionsValue = null,
            [FromQuery]string DMFilePath = null)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = priority,
                Schedule = schedule
            };
            return Ok(await queuer.GenerateDocumentReportByMenuID(aid, bid, documentReportType, menuID, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath));
        }

        [HttpGet]
        [Route("DocumentReport/{aid}/{bid}/{documentReportType}/template/{templateName}")]
        public async Task<IActionResult> GenerateDocumentReportByTemplateName(
            byte aid,
            short bid,
            byte documentReportType,
            string templateName,
            [FromQuery]string priority,
            [FromQuery]string schedule,
            [FromQuery]int[] DataID = null,
            [FromQuery]int[] CustomFields = null,
            [FromQuery]string OrderStatusFilter = null,
            [FromQuery]string OptionsValue = null,
            [FromQuery]string DMFilePath = null)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = priority,
                Schedule = schedule
            };
            return Ok(await queuer.GenerateDocumentReportByTemplateName(aid, bid, documentReportType, templateName, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath));
        }

        [HttpGet]
        [Route("reconciliate/{bid}/{locationID}/{enteredById}/{accountingDT}/{createAdjustments}/{cashDrawer}")]
        public async Task<IActionResult> Reconciliate(
            short bid,
            byte locationID,
            short enteredById,
            DateTime accountingDT,
            bool createAdjustments,
            string cashDrawer,
            [FromQuery]string priority,
            [FromQuery]string schedule)
        {
            var queuer = new HangfireTaskQueuer()
            {
                Priority = priority,
                Schedule = schedule
            };

            return Ok(await queuer.Reconciliate(
                bid, 
                locationID == 0 ? (byte?)null : locationID, 
                enteredById == 0 ? (short?)null : 
                enteredById, 
                accountingDT, 
                createAdjustments, 
                String.IsNullOrWhiteSpace(cashDrawer) ? null : JsonConvert.DeserializeObject<IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)>>(Encoding.UTF8.GetString(Convert.FromBase64String(cashDrawer)))));
        }

        private void FailDoingStuff()
        {
            Models.BusinessData businessData = null;

            short id = businessData.BillingEmployeeID.Value;
        }
    }
}
