﻿using Hangfire.Annotations;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Text;

namespace Endor.BGEngine.Core.Classes
{
    public class HangfireDashboardAuthorizationFilter : IDashboardAuthorizationFilter
    {
        private readonly string _userName;
        private readonly string _password;
        
        public HangfireDashboardAuthorizationFilter(IConfigurationRoot configuration)
        {
            this._userName = configuration["Hangfire:UserName"];
            this._password = configuration["Hangfire:Password"];
        }

        public bool Authorize([NotNull] DashboardContext context)
        {
            HttpContext httpContext = context.GetHttpContext();

            string authHeader = httpContext.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic "))
            {
                // Get the encoded username and password
                var encodedUsernamePassword = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                // Decode from Base64 to string
                var decodedUsernamePassword = Encoding.UTF8.GetString(Convert.FromBase64String(encodedUsernamePassword));
                // Split username and password
                var username = decodedUsernamePassword.Split(':', 2)[0];
                var password = decodedUsernamePassword.Split(':', 2)[1];

                // Check if login is correct
                return IsAuthorized(username, password);
            }
            // Return authentication type (causes browser to show login dialog)
            httpContext.Response.Headers["WWW-Authenticate"] = "Basic";

            return false;
        }

        private bool IsAuthorized(string username, string password)
        {
            // Check that username and password are correct
            return username.Equals(this._userName, StringComparison.InvariantCultureIgnoreCase)
                   && password.Equals(this._password);
        }
    }
}
