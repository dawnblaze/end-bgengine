﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Endor.BGEngine.Core.Classes;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.Extensions.Configuration;
using DbType = Endor.Tasks.DbType;

namespace Endor.BGEngine.Core.Web.Classes
{
    /// <inheritdoc />
    public class DBBackupTask : IDBBackup
    {
        private readonly ITenantDataCache _cache;
        private readonly RemoteLogger _logger;

        /// <summary>
        /// DataBase Backup Task
        /// </summary>
        /// <param name="tenantData"></param>
        /// <param name="logger"></param>
        public DBBackupTask(ITenantDataCache tenantData, RemoteLogger logger)
        {
            this._cache = tenantData;
            this._logger = logger;
        }

        /// <summary>
        /// Creates a DataBase Backup
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="dbType"></param>
        /// <returns></returns>
        public async Task CreateDBBackup(short bid, DbType dbType)
        {
            var tenantData = await this._cache.Get(bid);
            var connectionString = tenantData.BusinessDBConnectionString;
            var storageConnectionString = tenantData.StorageConnectionString;
            switch (dbType)
            {
                case DbType.Master:
                    connectionString = ScheduledTask.AuthContextConnectionString;
                    break;
                case DbType.Logging:
                    connectionString = tenantData.LoggingDBConnectionString;
                    break;
            }



        }
    }
}
