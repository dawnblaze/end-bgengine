﻿using Endor.AzureStorage;
using Endor.Logging.Client;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Classes
{   
    public class DocCleaner : IDocCleaner
    {
        private readonly ITenantDataCache _cache;
        private readonly RemoteLogger _logger;

        public DocCleaner(ITenantDataCache cache, RemoteLogger logger)
        {
            this._cache = cache;
            this._logger = logger;
        }

        public async Task EmptyTemp(short bid)
        {
            try
            {
                var cache = await _cache.Get(bid);

                var storageClient = new EntityStorageClient(cache.StorageConnectionString, bid);

                await storageClient.EmptyTemp();
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in DocCleaner.EmptyTemp", ex);
            }
        }

        public async Task EmptyTrash(short bid)
        {
            try
            {
                var cache = await _cache.Get(bid);

                var storageClient = new EntityStorageClient(cache.StorageConnectionString, bid);

                await storageClient.EmptyTrash();
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in DocCleaner.EmptyTrash", ex);
            }
        }
    }
}