﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Web.Classes
{
    public class OptionsDefinition
    {
        public int SortIndex { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public EnumElementType ElementType { get; set; }
        public string DefaultValue { get; set; }
        public bool IsRequired { get; set; }
        public bool InUse { get; set; }
    }

    public enum EnumElementType
    {
        None = 0,
        Checkbox = 1,
        Number = 2,
        SingleLineString = 3
    }

    public enum EnumReportingDocumentFormat
    {
        /// <summary>
        /// Returns the Template converted to Word Docx format.  This is the default if the template is not specified.
        /// </summary>
        docx = 1,
        /// <summary>
        /// Returns the Template converted to Adobe PDF format.
        /// </summary>
        pdf = 2,
        /// <summary>
        /// Returns the Template in internal TX format.
        /// </summary>
        tx = 3,
        /// <summary>
        /// Returns the JSON config file without the Template.
        /// </summary>
        config = 4
    }
}
