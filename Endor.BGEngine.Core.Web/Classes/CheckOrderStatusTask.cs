﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Common.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Hangfire;
using Hangfire.Console;
using Hangfire.Server;
using Hangfire.States;
using Microsoft.EntityFrameworkCore;

namespace Endor.BGEngine.Core.Classes
{
    /// <summary>
    /// Check Order Status Task
    /// </summary>
    public class CheckOrderStatusTask : ICheckOrderStatus
    {
        #region Private Properties

        private readonly IRTMPushClient _rtmClient;
        private readonly RemoteLogger _logger;
        private readonly ITenantDataCache _cache;
        private readonly IIndexer _indexer;
        private readonly string[] orderIncludes = { "Items", "Items.OrderItemStatus" };
        private readonly OrderOrderStatus[] unchangeableStatuses = { OrderOrderStatus.OrderInvoiced, OrderOrderStatus.OrderClosed, OrderOrderStatus.OrderVoided };
        private ApiContext _apiContext;

        #endregion

        /// <summary>
        /// Check Order Status Task
        /// </summary>
        /// <param name="rtmClient">Messaging Client</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="cache">Tenant Data Cache</param>
        public CheckOrderStatusTask(IRTMPushClient rtmClient, RemoteLogger logger, ITenantDataCache cache, IIndexer indexer)
        {
            this._rtmClient = rtmClient;
            this._logger = logger;
            this._cache = cache;
            this._indexer = indexer;
        }

        /// <summary>
        /// Checks the Order's Status to see if it needs to be updated based on its Line Item's statuses
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderId">Order ID</param>
        /// <param name="status">Status ID</param>
        /// <param name="context">Perform Context</param>
        /// <returns></returns>
        public async Task CheckOrderStatus(short bid, int orderId, int status, PerformContext context)
        {
            try
            {
                context.WriteLine($"Order Status Check started for ({bid}, {orderId})");

                using (_apiContext = new ApiContext(this._cache, bid))
                {
                    TransactionHeaderData order = await _apiContext.TransactionHeaderData
                            .IncludeAll(orderIncludes)
                            .FirstOrDefaultAsync(o => o.BID == bid && o.ID == orderId);

                    OrderOrderStatus oldOrderStatus = order.OrderStatusID;

                    OrderItemStatus lowestOrderItemStatus = order.Items
                        ?.OrderBy(i => i.OrderItemStatus.OrderStatusID)
                        .Select(i => i.OrderItemStatus).FirstOrDefault();

                    if (lowestOrderItemStatus?.OrderStatusID != null && lowestOrderItemStatus.OrderStatusID != oldOrderStatus)
                    {
                        var lowestOrderItemStatusID = (OrderOrderStatus)lowestOrderItemStatus.OrderStatusID;

                        // order cannot move out of Invoice, Closed, or Voided statuses
                        if (unchangeableStatuses.Contains(oldOrderStatus) && !unchangeableStatuses.Contains(lowestOrderItemStatusID))
                        {
                            context.WriteLine($"Order Status Check completed for ({bid}, {orderId}): cannot move out of Invoice, Closed, or Voided");
                            return;
                        }

                        order.OrderStatusID = lowestOrderItemStatusID;
                        await _apiContext.SaveChangesAsync();

                        await TryRecomputeGL(bid, orderId, status, "Order Status Change", "Order Status Change", null, null, (byte)GLEntryType.Order_Status_Change, _logger);

                        await Task.WhenAll(
                            _rtmClient.SendRefreshMessage(new RefreshMessage()
                            {
                                RefreshEntities = new List<RefreshEntity>()
                                {
                                    new RefreshEntity()
                                    {
                                        ClasstypeID = ClassType.Order.ID(),
                                        ID = orderId,
                                        DateTime = DateTime.UtcNow,
                                        RefreshMessageType = RefreshMessageType.Change,
                                        BID = bid,
                                    }
                                }
                            }),
                            this._indexer.Index(bid, ClassType.Order.ID(), orderId)
                        );
                    }
                }

                context.WriteLine($"Order Status Check completed for ({bid}, {orderId})");
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in CheckOrderStatusTask.CheckOrderStatus", ex);
            }
        }

        private static async Task TryRecomputeGL(short bid, int orderId, int status, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType, RemoteLogger logger)
        {
            try
            {
                // recompute GL
                var queuer = new HangfireTaskQueuer()
                {
                    Priority = "medium"
                };
                await queuer.RecomputeGL(bid, (byte)GLEngine.Models.EnumGLType.Order, orderId, name, Subject, CompletedById, CompletedByContactId, glEntryType);
            }
            catch(Exception ex)
            {
                await logger.Error(bid, "Error in CheckOrderStatusTask.CheckOrderStatus.TryRecomputeGL", ex);
            }
        }
    }
}