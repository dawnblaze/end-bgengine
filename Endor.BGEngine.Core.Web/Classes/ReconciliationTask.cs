﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Common.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tenant;
using Hangfire.Console;
using Hangfire.Server;
using Microsoft.EntityFrameworkCore;

namespace Endor.BGEngine.Core.Web.Classes
{
    public class ReconciliationTask : Tasks.IReconciliation
    {
        private readonly ITenantDataCache _cache;
        private readonly RemoteLogger _logger;

        public ReconciliationTask(RemoteLogger logger, ITenantDataCache cache)
        {
            _logger = logger;
            _cache = cache;
        }

        public async Task Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments, IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer, string priority, PerformContext context)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();
                using (var ctx = new ApiContext(optionsBuilder.Options, _cache, (short)bid))
                {
                    DateTime completionDate = DateTime.UtcNow;
                    var glEngine = new GLEngine.GLEngine(bid, ctx);
                    var reconciliations = await glEngine.Reconcile(locationID, enteredById, accountingDT, createAdjustments, 
                        cashDrawer != null && cashDrawer.Any() ? cashDrawer.Select(a => new GLEngine.Classes.CashDrawerAdjustment { 
                            AdjustmentsIn = a.CashIn,
                            AdjustmentsOut = a.CashOut,
                            DepositTransfer = a.CashDeposit,
                            LocationID = a.LocationID
                        }).ToList() : null, 
                        AtomCRUDService<Reconciliation, int>.RequestIDIntegerAsync);
                    await ctx.SaveChangesAsync();
                    context.WriteLine($"Reconciliate created ({bid}, {reconciliations.Count()} items)");
                }
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in ReconciliationTask.Reconciliate", ex);
            }
        }
    }
}
