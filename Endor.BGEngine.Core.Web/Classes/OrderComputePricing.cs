﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Common.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Hangfire;
using Hangfire.Console;
using Hangfire.Server;
using Hangfire.States;
using Microsoft.EntityFrameworkCore;

namespace Endor.BGEngine.Core.Classes
{
    /// <summary>
    /// Order Compute Pricing
    /// </summary>
    public class OrderComputePricing : IOrderPricingCompute
    {
        #region Private Properties

        private readonly IRTMPushClient _rtmClient;
        private readonly RemoteLogger _logger;
        private readonly ITenantDataCache _cache;
        private readonly string[] orderIncludes = { "Items", "Items.Components", "Items.Surcharges" };

        private ApiContext _apiContext = null;

        #endregion

        /// <summary>
        /// Order Compute Pricing
        /// </summary>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="cache">Tenant Data Cache</param>
        public OrderComputePricing(IRTMPushClient rtmClient, RemoteLogger logger, ITenantDataCache cache)
        {
            this._rtmClient = rtmClient;
            this._logger = logger;
            this._cache = cache;
        }

        /// <summary>
        /// A call to compute the Order Pricing without Tax
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderID">Order ID</param>
        /// <param name="priority">Priority</param>
        /// <param name="context">Perform Context</param>
        /// <returns></returns>
        public async Task OrderPricingCompute(short bid, int orderID, string priority, PerformContext context)
        {
            try
            {
                (OrderPriceResult result, TransactionHeaderData order) = await ComputeOrderPricing(bid, orderID, false, context);
                
                if (result != null)
                {
                    await UpdateOrderFromPriceResult(result, order, false);
                    await SendRefresh(bid, orderID, false, context);
                }
                else
                {
                    await _logger.Information(bid, $"Error in OrderComputePricing.OrderPricingCompute.");
                    context.WriteLine($"Error in OrderComputePricing.OrderPricingCompute. OrderPriceResult returned null.");
                }
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains($"Order with ID `{ orderID}` not found."))
                {
                    await _logger.Information(bid, $"Error in OrderComputePricing.OrderPricingCompute: Order with ID {orderID} not found.");
                    context.WriteLine(ConsoleTextColor.Red, $"Error in OrderComputePricing.OrderPricingCompute: Order with ID {orderID} not found.");                    
                    BackgroundJob.Delete(context.BackgroundJob.Id);
                }
                else
                {
                    await _logger.Information(bid, $"Error in OrderComputePricing.OrderPricingCompute: {ioe.Message}");
                    context.WriteLine(ConsoleTextColor.Red, $"Error in OrderComputePricing.OrderPricingCompute: {ioe.Message}");
                }
                throw;
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in OrderComputePricing.OrderPricingCompute", ex);
                throw;
            }
        }

        /// <summary>
        /// A call to compute the Order Pricing with Tax
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderID">Order ID</param>
        /// <param name="priority">Priority</param>
        /// <param name="context">Perform Context</param>
        /// <returns></returns>
        public async Task OrderPricingComputeTax(short bid, int orderID, string priority, PerformContext context)
        {
            try
            {
                (OrderPriceResult result, TransactionHeaderData order) = await ComputeOrderPricing(bid, orderID, true, context);

                if (result != null)
                {
                    await UpdateOrderFromPriceResult(result, order, true);
                    await SendRefresh(bid, orderID, true, context);
                }
                else
                {
                    await _logger.Information(bid, $"Error in OrderComputePricing.OrderPricingComputeTax.");
                    context.WriteLine($"Error in OrderComputePricing.OrderPricingCompute. OrderPriceResult returned null.");
                }
            }
            catch (InvalidOperationException ioe)
            {
                if (ioe.Message.Contains($"Order with ID `{ orderID}` not found."))
                {
                    await _logger.Information(bid, $"Error in OrderComputePricing.OrderPricingComputeTax: Order with ID {orderID} not found.");
                    context.WriteLine(ConsoleTextColor.Red, $"Error in OrderComputePricing.OrderPricingComputeTax: Order with ID {orderID} not found.");
                    BackgroundJob.Delete(context.BackgroundJob.Id, "failed");
                }
                else
                {
                    await _logger.Information(bid, $"Error in OrderComputePricing.OrderPricingComputeTax: {ioe.Message}");
                    context.WriteLine(ConsoleTextColor.Red, $"Error in OrderComputePricing.OrderPricingComputeTax: {ioe.Message}");
                }
                throw;
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in OrderComputePricing.OrderPricingComputeTax", ex);
                throw;
            }
        }

        /// <summary>
        /// Compute's the Order Pricing
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderID">Order ID</param>
        /// <param name="computeTax">If Tax should be computed</param>
        /// <param name="performContext">Perform Context</param>
        /// <returns></returns>
        private async Task<(OrderPriceResult result, TransactionHeaderData order)> ComputeOrderPricing(short bid, int orderID, bool computeTax, PerformContext performContext)
        {
            performContext?.WriteLine($"Order Pricing {(computeTax ? "Tax Computation" : "Computation")} for ({bid}, {orderID}) started.");

            OrderPriceResult result = null;
            TransactionHeaderData order = null;

            try
            {
                _apiContext = new ApiContext(this._cache, bid);
                OrderPriceRequest request;
                (request, order) = await GetOrderPriceRequest(bid, orderID);

                if (request != null)
                {
                    PricingEngine pricingEngine = new PricingEngine(bid, _apiContext, _cache, _logger);
                    result = await pricingEngine.Compute(request, computeTax);
                }
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return (result, order);
        }

        #region Handling of ToPriceRequests

        /// <summary>
        /// This will look up an Order and build out an OrderPriceRequest
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderID">Order ID</param>
        /// <param name="apiContext">Api Context</param>
        /// <returns></returns>
        private async Task<(OrderPriceRequest request, TransactionHeaderData order)> GetOrderPriceRequest(short bid, int orderID)
        {
            TransactionHeaderData order = await _apiContext.TransactionHeaderData.IncludeAll(orderIncludes)
                                .FirstOrDefaultAsync(o => o.BID == bid && o.ID == orderID);
            if (order == null)
                throw new InvalidOperationException($"Order with ID `{orderID}` not found.");

            OrderPriceRequest request = new OrderPriceRequest()
            {
                Destinations = null,
                TaxNexusList = null,
                DiscountList = null,
                FinanceCharge = order.PriceFinanceCharge
            };

            if (order.Items != null && order.Items.Count > 0)
            {
                request.Items = new List<ItemPriceRequest>();
                foreach (var item in order.Items)
                    request.Items.Add(await ItemToPriceRequest(item, order.CompanyID));
            }

            return (request, order);
        }

        /// <summary>
        /// Converts an OrderItem to an ItemPriceRequest
        /// </summary>
        /// <param name="item">Order Item Data</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="apiContext">Api Context</param>
        /// <returns></returns>
        private async Task <ItemPriceRequest> ItemToPriceRequest(OrderItemData item, int companyID)
        {
            ItemPriceRequest itemRequest = new ItemPriceRequest()
            {
                TaxNexusList = null,
                ItemDiscountAmount = item.PriceItemDiscount,
                ItemDiscountPercent = item.PriceAppliedOrderDiscountPercent,
                TaxInfoList = null,
                IsOutsourced = item.IsOutsourced,
                Quantity = item.Quantity,
                EngineType = PricingEngineType.SimplePart,
                CompanyID = companyID,
                UnitPriceOV = item.PriceUnitOV,
                UnitPrice = item.PriceUnitPreTax
            };

            if (item.Components != null && item.Components.Count > 0)
            {
                itemRequest.Components =  new List<ComponentPriceRequest>();
                foreach (var component in item.Components)
                    itemRequest.Components.Add(await ComponentToPriceRequest(component, companyID));
            }

            if (item.Surcharges != null && item.Surcharges.Count > 0)
            {
                itemRequest.Surcharges = new List<SurchargePriceRequest>();
                foreach (var surcharge in item.Surcharges)
                    itemRequest.Surcharges.Add(SurchargeToPriceRequest(surcharge, companyID, item.Quantity));
            }

            return itemRequest;
        }

        /// <summary>
        /// Converts a Component to a ComponentPriceRequest
        /// </summary>
        /// <param name="component">Order Item Component</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="apiContext">Api Context (used to retrieve ChildComponents)</param>
        /// <returns></returns>
        private async Task<ComponentPriceRequest> ComponentToPriceRequest(OrderItemComponent component, int companyID)
        {
            ComponentPriceRequest componentRequest = new ComponentPriceRequest()
            {
                ComponentType = component.ComponentType,
                ComponentID = component.ComponentID,
                TotalQuantity = component.TotalQuantity,
                TotalQuantityOV = component.TotalQuantityOV,
                PriceUnit = component.PriceUnit,
                PriceUnitOV = component.PriceUnitOV,
                Variables = null, // makes the pricerequest null on api --> this.getVariablesDictionary(c.AssemblyDataJSON),
                TaxNexusList = null,
                TaxInfoList = null,
                CompanyID = companyID,
            };

            component.ChildComponents = await _apiContext.OrderItemComponent.Where(c => c.BID == component.BID && c.ParentComponentID == component.ID).ToListAsync();
            if (component.ChildComponents != null && component.ChildComponents.Count > 0)
            {
                componentRequest.ChildComponents = new List<ComponentPriceRequest>();
                foreach (var child in component.ChildComponents)
                    componentRequest.ChildComponents.Add(await ComponentToPriceRequest(child, companyID));
            }

            return componentRequest;
        }

        /// <summary>
        /// Converts a Surcharge to a SurchargePriceRequest
        /// </summary>
        /// <param name="surcharge">Order Item Surcharge</param>
        /// <param name="companyID">Company ID</param>
        /// <param name="quantity">Quantity</param>
        /// <returns></returns>
        private SurchargePriceRequest SurchargeToPriceRequest(OrderItemSurcharge surcharge, int companyID, decimal quantity)
        {
            return new SurchargePriceRequest()
            {
                SurchargeDefID = surcharge.SurchargeDefID,
                TaxabilityCode = surcharge.TaxCodeID,
                Quantity = quantity,
                PriceFixedAmount = surcharge.DefaultFixedFee,
                PricePerUnitAmount = surcharge.DefaultPerUnitFee,
                PricePreTax = surcharge.PricePreTax,
                PricePreTaxOV = surcharge.PriceIsOV,
                TaxNexusList = null,
                TaxInfoList = null,
                CompanyID = companyID
            };
        }

        #endregion

        #region Handling of FromPriceResult

        /// <summary>
        /// Updates the Order based on the OrderPriceResult
        /// </summary>
        /// <param name="result">Order Price Result</param>
        /// <returns></returns>
        private async Task UpdateOrderFromPriceResult(OrderPriceResult result, TransactionHeaderData order, bool computeTax)
        {
            order.CostLabor = result.CostLabor;
            order.CostMachine = result.CostMachine;
            order.CostMaterial = result.CostMaterial;
            order.PriceProductTotal = result.ItemPreTaxTotal;
            order.PriceDestinationTotal = result.DestPreTaxTotal;
            order.PriceFinanceCharge = result.FinanceCharge;
            order.PriceDiscount = result.PriceOrderDiscountTotal;
            order.PriceTaxable = result.PriceTaxable;
            order.PriceTax = result.TaxAmount;

            if (computeTax)
                order.PriceTax = result.TaxAmount;

            if (result.Items != null && result.Items.Count > 0)
            {
                List<OrderItemData> orderItems = order.Items.ToList();
                for (int i = 0; i < result.Items.Count; i++)
                {
                    await UpdateItemFromPriceResult(result.Items[i], orderItems[i], computeTax);
                }
            }

            try
            {
                await _apiContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updates the OrderItem based on the ItemPriceResult
        /// </summary>
        /// <param name="result">Item Price Result</param>
        /// <param name="item">Order Item</param>
        /// <returns></returns>
        private async Task UpdateItemFromPriceResult(ItemPriceResult result, OrderItemData item, bool computeTax)
        {
            item.CostLabor = result.CostLabor;
            item.CostMachine = result.CostMachine;
            item.CostMaterial = result.CostMaterial;
            item.PriceTax = result.TaxAmount;
            item.PriceAppliedOrderDiscount = result.AppliedOrderDiscountAmount;
            item.PriceAppliedOrderDiscountPercent = result.AppliedOrderDiscountPercent;
            item.PriceComponent = result.PriceComponent;
            item.PriceSurcharge = result.PriceSurcharge;
            item.PriceUnitOV = result.UnitPriceOV;

            if ((result.Surcharges?.Count ?? 0) > 0)
            {
                // Delete unused surcharges
                for (int i = item.Surcharges.Count - 1; i >= 0; i--)
                {
                    OrderItemSurcharge itemSurcharge = item.Surcharges.ElementAt(i);
                    if (!result.Surcharges.Any(x => x.SurchargeDefID == itemSurcharge.SurchargeDefID))
                    {
                        item.Surcharges.Remove(itemSurcharge);
                        _apiContext.OrderItemSurcharge.Remove(itemSurcharge);
                    }
                }

                int previousItemLength = item.Surcharges.Count;

                foreach (SurchargePriceResult surchargeResult in result.Surcharges)
                {
                    OrderItemSurcharge itemSurcharge = item.Surcharges.FirstOrDefault(x => x.SurchargeDefID == surchargeResult.SurchargeDefID);

                    if (itemSurcharge == null)
                    {
                        SurchargeDef surchargeDef = _apiContext.SurchargeDef.FirstOrDefault(x => x.BID == item.BID && x.ID == (short)(surchargeResult.SurchargeDefID));

                        if (surchargeDef != null)
                        {
                            itemSurcharge = new OrderItemSurcharge()
                            {
                                BID = item.BID,
                                ID = await AtomCRUDService<OrderItemSurcharge, int>.RequestIDIntegerAsync(_apiContext, item.BID, (int)ClassType.OrderItemSurcharge),
                                Name = surchargeDef.Name,
                                OrderItemID = item.ID,
                                SurchargeDefID = surchargeResult.SurchargeDefID,
                                Number = (short)(previousItemLength + 1),
                                PriceIsOV = false,
                                DefaultFixedFee = surchargeDef.DefaultFixedFee,
                                DefaultPerUnitFee = surchargeDef.DefaultPerUnitFee,
                                IncomeAccountID = surchargeDef.IncomeAccountID,
                                TaxCodeID = surchargeDef.TaxCodeID,
                            };

                            item.Surcharges.Add(itemSurcharge);
                        }
                    }

                    if (itemSurcharge != null)
                        itemSurcharge.PricePreTax = surchargeResult.PricePreTax.GetValueOrDefault(0m);
                }
            }

            else if (item.Surcharges.Count > 0)
            {
                _apiContext.OrderItemSurcharge.RemoveRange(item.Surcharges);
                item.Surcharges.Clear();
            }

            if (computeTax)
                item.PriceTax = result.TaxAmount;
        }

        #endregion

        /// <summary>
        /// Sends a Refresh Message and outputs completion
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderID">Order ID</param>
        /// <param name="computeTax">If Tax was computed</param>
        /// <param name="context">Perform Context</param>
        /// <returns></returns>
        private async Task SendRefresh(short bid, int orderID, bool computeTax, PerformContext context)
        {
            await _rtmClient.SendRefreshMessage(new RTM.Models.RefreshMessage()
            {
                RefreshEntities = DoGetRefreshMessagesOnUpdate(bid, orderID)
            });

            context?.WriteLine($"Order Pricing {(computeTax ? "Tax Computation" : "Computation")} for ({bid}, {orderID}) completed.");
        }

        /// <summary>
        /// Gets the RefreshEntity list for a RefreshMessage
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderId">Order ID</param>
        /// <returns></returns>
        private List<RefreshEntity> DoGetRefreshMessagesOnUpdate(short bid, int orderId)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    ClasstypeID = ClassType.Order.ID(),
                    ID = orderId,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Change,
                    BID = bid,
                }
            };
        }

    }
}