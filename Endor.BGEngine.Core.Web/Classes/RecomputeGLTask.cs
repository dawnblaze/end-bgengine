﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Endor.Api.Common.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Hangfire.Console;
using Hangfire.Server;
using Microsoft.EntityFrameworkCore;

namespace Endor.BGEngine.Core.Classes
{
    public class RecomputeGLTask : IRecomputeGL
    {
        private readonly IRTMPushClient _rtmClient;
        private readonly ITenantDataCache _cache;
        private readonly RemoteLogger _logger;

        public RecomputeGLTask(IRTMPushClient rtmClient, ITenantDataCache tenantData, RemoteLogger logger)
        {
            this._rtmClient = rtmClient;
            this._cache = tenantData;
            this._logger = logger;
        }

        public async Task RecomputeGL(short bid, byte EnumGLType, int Id, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType, string priority, PerformContext context)
        {
            try
            {
                GLEngine.Models.EnumGLType gLType = (GLEngine.Models.EnumGLType)EnumGLType;
                var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();
                using (var ctx = new ApiContext(optionsBuilder.Options, _cache, (short)bid))
                {
                    DateTime completionDate = DateTime.UtcNow;
                    var glEngine = new GLEngine.GLEngine(bid, ctx);
                    var glData = glEngine.ComputeGLDiff(gLType, Id);
                    if (glData.Count > 0)
                    {
                        int id = await AtomCRUDService<ActivityGlactivity, int>.RequestIDIntegerAsync(ctx, bid, (int)ClassType.GLActivity);
                        var activity = await glEngine.InsertGLActivity(name, Subject, CompletedById, CompletedByContactId, completionDate, (GLEntryType)Enum.ToObject(typeof(GLEntryType), glEntryType), Id, glData, AtomCRUDService<ActivityGlactivity, int>.RequestIDIntegerAsync);
                    }
                }

                context.WriteLine($"RecomputeGL Computed for ({bid}, {gLType.ToString()}, {Id}) started.");
                await _rtmClient.SendRefreshMessage(new RTM.Models.RefreshMessage()
                {
                    RefreshEntities = DoGetRefreshMessagesOnUpdate(bid, Id)
                });
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in RecomputeGLTask.RecomputeGL", ex);
            }
        }

        private List<RefreshEntity> DoGetRefreshMessagesOnUpdate(short bid, int orderId)
        {
            return new List<RefreshEntity>()
            {
                new RefreshEntity()
                {
                    ClasstypeID = ClassType.Order.ID(),
                    ID = orderId,
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Change,
                    BID = bid,
                },
                new RefreshEntity()
                {
                    ClasstypeID = ClassType.Order.ID(),
                    DateTime = DateTime.UtcNow,
                    RefreshMessageType = RefreshMessageType.Change,
                    BID = bid,
                }
            };
        }

    }
}