﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.AzureStorage;
using Endor.BGEngine.Core.Classes;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;

namespace Endor.BGEngine.Core.Web.Classes
{
    public class AssemblyCleaner : IAssemblyCleaner
    {
        private readonly ITenantDataCache _cache;
        private readonly CacheOptions _options;
        private readonly RemoteLogger _logger;

        public AssemblyCleaner(ITenantDataCache cache, CacheOptions options, RemoteLogger logger)
        {
            this._cache = cache;
            this._options = options;
            this._logger = logger;
        }

        private async Task<bool> ClearDMItem(EntityStorageClient storageClient, DMItem doc, int ctid)
        {
            if (doc.IsFolder)
            {
                bool deleteFolder = true;
                foreach (DMItem childDoc in doc.Contents)
                {
                    if (!await ClearDMItem(storageClient, childDoc, ctid))
                        deleteFolder = false;
                }

                if (deleteFolder)
                {
                    string[] pathParts = doc.Path.Split("/");

                    if (pathParts.Length >= 1 && pathParts[0] != "")
                    {
                        if (int.TryParse(pathParts[0], out int id))
                        {
                            DMID dmid = new DMID { ctid = ctid, id = id };
                            int x = await storageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, dmid, $"{doc.Name}/_");
                            return (x > 0);
                        }
                    }
                }

                return false;
            }

            else
            {
                string[] pathParts = doc.Path.Split("/");

                if (pathParts.Length >= 2 
                    && ((pathParts[1] == "bin" && doc.Name.EndsWith(".dll")) 
                    //|| (pathParts[1] == "source" && doc.Name.EndsWith(".cs"))
                        ))  
                {
                    if (int.TryParse(pathParts[0], out int id))
                    {
                        DMID dmid = new DMID { ctid = ctid, id = id };
                        int x = await storageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, dmid, $"{pathParts[1]}/{doc.Name}");
                        return (x > 0);
                    }
                }

                return false;
            }
        }

        private async Task ClearDMItems(EntityStorageClient storageClient, int ctid)
        {
            DMID dmid = new DMID() { ctid = ctid };
            List<DMItem> docs = await storageClient.GetDocumentList(StorageBin.Permanent, Bucket.Data, dmid, false, true);

            foreach (var doc in docs)
                await ClearDMItem(storageClient, doc, ctid);
        }

        public async Task ClearAllAssemblies(short bid)
        {
            try
            {
                var cache = await _cache.Get(bid);

                EntityStorageClient storageClient = new EntityStorageClient(cache.StorageConnectionString, bid);

                await ClearDMItems(storageClient, ClassType.Assembly.ID());
                await ClearDMItems(storageClient, ClassType.Machine.ID());
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in AssemblyCleaner.ClearAllAssemblies", ex);
            }
        }

        public async Task ClearAssembly(short bid, int ctid, int id)
        {
            try
            {
                var cache = await _cache.Get(bid);

                EntityStorageClient storageClient = new EntityStorageClient(cache.StorageConnectionString, bid);

                DMID dmid = new DMID() { ctid = ctid };
                List<DMItem> docs = await storageClient.GetDocumentList(StorageBin.Permanent, Bucket.Data, dmid, false, true);

                DMItem doc = docs.FirstOrDefault(x => x.Name == id.ToString());

                if (doc != null)
                    await ClearDMItem(storageClient, doc, ctid);

                if(ctid == ClassType.Assembly.ID())
                    if(IsMachineTemplate(bid, id, out int?[] machineIDs))
                        foreach(int machineID in machineIDs)
                            await ClearAssembly(bid, ClassType.Machine.ID(), machineID);
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in AssemblyCleaner.ClearAssembly", ex);
            }
        }

        /// <summary>
        /// handles checking if assembly is a machine template
        /// outputs the machine ids that are using the machine template
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="id"></param>
        /// <param name="machineIDs"></param>
        /// <returns></returns>
        private bool IsMachineTemplate(short bid, int id, out int?[] machineIDs)
        {
            machineIDs = null;

            var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();
            using (ApiContext apiContext = new ApiContext(optionsBuilder.Options, _cache, bid))
            {
                AssemblyData possibleMachineTemplate = apiContext.AssemblyData.Find(bid, id);
                if (possibleMachineTemplate == null)
                    return false;

                if (possibleMachineTemplate.AssemblyType != AssemblyType.MachineTemplate)
                    return false;

                machineIDs = apiContext.MachineData.Where(m => m.BID == bid && m.MachineTemplateID == id).Select(m => (int?)m.ID).ToArray();
            }
            

            return true;
        }

    }
}
