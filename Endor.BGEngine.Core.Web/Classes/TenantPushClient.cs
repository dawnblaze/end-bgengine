﻿using Endor.RTM;
using Endor.RTM.Models;
using Endor.Tenant;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Classes
{

    public class TenantPushClient : IRTMPushClient
    {
        public ITenantDataCache _cache { get; set; }
        private RealtimeMessagingPushClient client { get; set; }
        public TenantPushClient(ITenantDataCache tenantData)
        {
            this._cache = tenantData;
        }

        private async Task<RealtimeMessagingPushClient> GetClient(short bid)
        {
            TenantData td = await _cache.Get(bid);

            //do caching
            return new RealtimeMessagingPushClient(td.MessagingURL);
        }
        public async Task SendRefreshMessage(RefreshMessage refreshMsg, string excludedConnectionId = null)
        {
            if (refreshMsg != null && refreshMsg.RefreshEntities != null && refreshMsg.RefreshEntities.Count > 0)
                await (await GetClient(refreshMsg.RefreshEntities.First().BID)).SendRefreshMessage(refreshMsg);
        }

        public Task SendSystemMessage(SystemMessage systemMsg)
        {
            // broadcast to all message servers
            throw new InvalidOperationException("I dont' know what servers to talk to because I have no BID or list of BIDs");

        }

        public Task SendUserMessage(UserMessage userMsg)
        {
            // Send to all messageservers where username is logged in.
            throw new InvalidOperationException("Needs user broadcast support");
        }

        public async Task SendEmployeeMessage(EmployeeMessage userMsg)
        {
            if(userMsg!=null)
            await (await GetClient(userMsg.BID)).SendEmployeeMessage(userMsg);
        }

        public async Task SendReportTemplateRefreshMessage(ReportTemplateRefreshMessage refreshMsg)
        {
            if (refreshMsg != null && refreshMsg.RefreshEntities != null && refreshMsg.RefreshEntities.Count > 0)
                await(await GetClient(refreshMsg.RefreshEntities.First().BID)).SendReportTemplateRefreshMessage(refreshMsg);
        }
    }
}