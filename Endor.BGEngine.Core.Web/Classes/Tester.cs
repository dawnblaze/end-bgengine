﻿using Endor.Logging.Client;
using Endor.Tasks;
using Hangfire.Console;
using Hangfire.Server;
using System;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Classes
{
    public class Tester: ITester
    {
        private readonly RemoteLogger _logger;
        public Tester(RemoteLogger logger)
        {
            this._logger = logger;
        }

        public async Task Test(short bid, PerformContext context)
        {
            try
            {
                if (context == null)
                    throw new InvalidOperationException("DI failed to provide context");

                context.WriteLine("Test Job Started");

                var progress = context.WriteProgressBar();

                for (int i = 0; i < 100; i++)
                {
                    await Task.Delay(100);
                    progress.SetValue(i + 1);
                }

                context.WriteLine("Test Job Ended");
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in Tester.Test", ex);
            }
        }
    }
}