﻿using Endor.Configuration;
using Hangfire.Common;
using Hangfire.States;
using Hangfire.Storage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Classes
{
    public class EmailOnJobFailureAttribute : JobFilterAttribute, IElectStateFilter, IApplyStateFilter
    {
        private static Dictionary<string, List<IState>> jobState = new Dictionary<string, List<IState>>();
        private ISMTPOptions _smtpOptions;
        private string _targetEmail;
        private static string failedMessageBody(ElectStateContext context)
        {
            return $@"Error Occurred in Background Job.
Job is moving to {context.CandidateState.Name}
Job { context.BackgroundJob.Id } : Created at { context.BackgroundJob.CreatedAt } failed due to {context.CandidateState.Reason}.";
        }
        private static string failedMessageBody(ApplyStateContext context)
        {
            return $@"Error Occurred in Background Job.
Job is moving to {context.NewState.Name}
Job { context.BackgroundJob.Id } : Created at { context.BackgroundJob.CreatedAt } failed due to {context.NewState.Reason}.";
        }
               

        private static string deletingMessageBody(ApplyStateContext context)
        {
            return failedMessageBody(context) + "\r\nIt is now being removed due to too many failures.";
        }

        public EmailOnJobFailureAttribute(ISMTPOptions options, string targetEmail)
        {
            this._smtpOptions = options;
            this._targetEmail = targetEmail;
        }

        public void OnStateElection(ElectStateContext context)
        {
            Debug.WriteLine("OnStateElection");
            Debug.WriteLine(JsonConvert.SerializeObject(context, new JsonSerializerSettings(){ReferenceLoopHandling = ReferenceLoopHandling.Ignore}));

            var failingState = context.CandidateState as FailedState;
            var deletingState = context.CandidateState as DeletedState;
            var successedState = context.CandidateState as SucceededState;

            if (!jobState.Keys.Contains(context.BackgroundJob.Id) && failingState != null)
            {
                jobState.Add(context.BackgroundJob.Id, new List<IState>());
            }
            else if (!jobState.Keys.Contains(context.BackgroundJob.Id) && failingState == null)
            {
                return;
            }

            if (failingState != null)
                jobState[context.BackgroundJob.Id].Add(failingState);


            // This will occur when the job is moving to failed and hasn't failed before            
            if (failingState != null && jobState[context.BackgroundJob.Id].Count == 1 && jobState[context.BackgroundJob.Id].First() == failingState)
            {
                SendEmailMessage(failedMessageBody(context));
            }


            if (successedState != null || deletingState != null)
            {
                jobState.Remove(context.BackgroundJob.Id);
            }
        }

        public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            if (context.NewState.Name == DeletedState.StateName && jobState.Keys.Contains(context.BackgroundJob.Id) && jobState[context.BackgroundJob.Id].All(t => t.Name == FailedState.StateName))
            {// this will occur when in final state and job had failed previously
                SendEmailMessage(deletingMessageBody(context));
                jobState.Remove(context.BackgroundJob.Id);
            }
        }


        public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            if (context.NewState.Name == DeletedState.StateName && jobState.Keys.Contains(context.BackgroundJob.Id) && jobState[context.BackgroundJob.Id].All(t => t.Name == FailedState.StateName))
            {
                // this will occur when in final state and job had failed previously
                SendEmailMessage(deletingMessageBody(context));
                jobState.Remove(context.BackgroundJob.Id);
            }
        }

        private void SendEmailMessage(string bodyMessage)
        {
            if (bodyMessage != null)
            {
                try
                {
                    var client = new SmtpClient(_smtpOptions.Host, _smtpOptions.Port);
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_smtpOptions.UserName, _smtpOptions.Password);

                    var mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(_smtpOptions.MailFromAddress);
                    mailMessage.To.Add(_targetEmail);
                    mailMessage.Body = bodyMessage;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Subject = "Failed EndorBGEngine Job";
                    Task messageTask = client.SendMailAsync(mailMessage);
                    messageTask.Wait(100);
                    if (messageTask.IsFaulted)
                    {
                        throw messageTask.Exception;
                    }
                }
                catch (Exception)
                {
                    //noop
                }

            }
        }
    }
}