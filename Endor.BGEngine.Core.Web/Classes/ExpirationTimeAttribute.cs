﻿using Hangfire.Common;
using Hangfire.States;
using Hangfire.Storage;
using System;

namespace Endor.BGEngine.Core.Classes
{
    public class ExpirationTimeAttribute : JobFilterAttribute, IApplyStateFilter
    {
        private int _expirationTime;

        /// <summary>
        /// Allows for setting the expiration time of a job or globally
        /// </summary>
        /// <param name="expirationTime">Expiration time in minutes</param>
        public ExpirationTimeAttribute(int expirationTime)
        {
            this._expirationTime = expirationTime;
        }

        public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            context.JobExpirationTimeout = TimeSpan.FromMinutes(_expirationTime);
        }

        public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            context.JobExpirationTimeout = TimeSpan.FromMinutes(_expirationTime);
        }
    }
}