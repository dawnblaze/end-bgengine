﻿using Endor.AzureStorage;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Models;
using System.Threading.Tasks;
using Endor.EF;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Endor.DocumentStorage.Models;
using Endor.Logging.Client;
using System;

namespace Endor.BGEngine.Core.Classes
{
    public class DeleteDraftsTask : IDeleteExpiredDrafts
    {
        private readonly HangfireTaskQueuer _queuer;
        private readonly ITenantDataCache _cache;
        private readonly RemoteLogger _logger;

        public DeleteDraftsTask(ITenantDataCache cache, RemoteLogger logger)
        {
            this._cache = cache;
            this._logger = logger;
            this._queuer = new HangfireTaskQueuer();
        }

        public async Task DeleteExpiredDrafts(short bid)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();

                using (var ctx = new ApiContext(optionsBuilder.Options, _cache, bid))
                {
                    var cache = await this._cache.Get(bid);

                    var storageClient = new EntityStorageClient(cache.StorageConnectionString, bid);
                    UserDraft myUserDraft = ctx.Set<UserDraft>().FirstOrDefault();

                    var userDraftData = await ctx.UserDraft.Where(ud => ud.IsExpired.Value == true).ToListAsync();

                    foreach (var data in userDraftData)
                    {
                        DMID source = new DMID() { id = data.ID, ctid = data.ClassTypeID };
                        await storageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, source, "draft.json");
                    }

                    ctx.UserDraft.RemoveRange(userDraftData);
                    await ctx.SaveChangesAsync();

                    foreach (var data in userDraftData)
                    {
                        await this._queuer.IndexModel(bid, data.ClassTypeID, data.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in DeleteDraftsTask.DeleteExpiredDrafts", ex);
            }
        }
    }
}