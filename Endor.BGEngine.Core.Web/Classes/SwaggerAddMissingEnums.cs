﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Endor.BGEngine.Core.Classes
{
    /// <summary>
    /// Swagger Add Missing Enums
    /// </summary>
    public class SwaggerAddMissingEnums : ISchemaFilter
    {
        /// <summary>
        /// Apply
        /// </summary>
        /// <param name="model">Model</param>
        /// <param name="context">Schema Filter Context</param>
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            //get the SystemType of our context
            var typeInfo = context.GetType().GetTypeInfo();
            string typeName = typeInfo.FullName;
            //if it's System.DateTime or System.String early exit
            if (typeName.StartsWith("System."))
                return;

            //get the names of properties that are enums mapped to the property infos for that property
            //we are going to use this to populate missing enums
            var propertyNameToPropertyInfo = typeInfo.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.PropertyType.IsEnum).ToDictionary(x => x.Name);

            //if the model has properties
            if (model.Properties != null)
            {
                //loop through them
                foreach (KeyValuePair<string, OpenApiSchema> propertyDictionaryItem in model.Properties)
                {
                    string propName = propertyDictionaryItem.Key;

                    if (propertyDictionaryItem.Value.Enum != null)
                    {
                        //remove it from our full dictionary of known enums
                        //because it is already defined, we don't want to define it twice
                        propertyNameToPropertyInfo.Remove(propName);
                    }

                    else
                    {
                        string propTypeName = propertyDictionaryItem.Value?.Type;

                        if (!String.IsNullOrWhiteSpace(propTypeName))
                        {
                            Type t = Type.GetType(propTypeName);
                            //if the property is an enum type
                            if (t != null && t.IsEnum)
                            {
                                //remove it from our full dictionary of known enums
                                //because it is already defined, we don't want to define it twice
                                propertyNameToPropertyInfo.Remove(propName);
                            }
                        }
                    }
                }
            }
            //now propertyNameToPropertyInfo only contains enum properties that do not yet exist in model.Properties
            //so if we have any enum properties left over
            if (propertyNameToPropertyInfo.Count > 0)
            {
                //iterate over them and add them into model.Properties
                foreach (var propNameAndTypeInfo in propertyNameToPropertyInfo)
                {
                    //deconstruct our KVP
                    string propertyNameThatIsEnumType = propNameAndTypeInfo.Key;
                    Type enumPropertyType = propNameAndTypeInfo.Value.PropertyType;

                    //now we want to list our enum values to put into the Enum property of the new Schema object
                    //we do this here because it takes several lines because Enum.GetValues returns just an Array object
                    //which makes it impossible to use Linq (or my intellisense is dumb)
                    List<IOpenApiAny> enumValues = new List<IOpenApiAny>();
                    foreach (var enumValue in Enum.GetValues(enumPropertyType))
                    {
                        //but we want string names, not ints so get the name and add that to our enumValues
                        // enumValues.Add(Enum.GetName(enumPropertyType, enumValue));
                    }

                    if (model.Properties != null)
                    {
                        //add to Properties using the propertyName and a new Schema
                        model.Properties.Add(propertyNameThatIsEnumType, new OpenApiSchema()
                        {
                            //because we use DescribeAllEnumsAsStrings, we don't use the typename, we use just "string"
                            //otherwise we'd have to make a proper class declaration in some other way
                            //this way we can just say "string but it has this list of acceptable values"
                            Type = "string",//enumPropertyType.Name,
                            Enum = enumValues
                        });
                        //now our model.Properties contains the missing enum property, enumerated with enum value names, not ints
                    }
                }
            }
        }
    }
}