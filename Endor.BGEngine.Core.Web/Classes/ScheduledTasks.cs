﻿using Endor.Tasks;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Endor.BGEngine.Core.Classes
{
    public class RepairUntrustedConstraint : ScheduledTask
    {
        private readonly ISQLMaintenance _sQLMaintenance;

        public override string Cron => "00 09 * * 0"; // At 9:00:00 every Sunday

        public RepairUntrustedConstraint(ISQLMaintenance sQLMaintenance)
        {
            this._sQLMaintenance = sQLMaintenance;
        }

        public override async Task Execute()
        {
            List<short> bids = await GetAllBIDs();

            foreach (var bid in bids)
            {
                await _sQLMaintenance.RepairUntrustedConstraint(bid);
            }
        }
    }

    public class EmptyTrash : ScheduledTask
    {
        private readonly IDocCleaner _docCleaner;

        public EmptyTrash(IDocCleaner docCleaner)
        {
            this._docCleaner = docCleaner;
        }

        public override async Task Execute()
        {
            List<short> bids = await GetAllBIDs();

            foreach (var bid in bids)
            {
                await _docCleaner.EmptyTrash(bid);
            }
        }

        public override string Cron
        {
            get
            {
                return "0 18 * * *"; // At 18:00:00 every day
            }
        }
    }

    public class EmptyTemp : ScheduledTask
    {
        private readonly IDocCleaner _docCleaner;

        public EmptyTemp(IDocCleaner docCleaner)
        {
            this._docCleaner = docCleaner;
        }

        public override string Cron => "15 18 * * *"; // At 18:15:00 every day

        public override async Task Execute()
        {
            List<short> bids = await GetAllBIDs();

            foreach (var bid in bids)
            {
                await _docCleaner.EmptyTemp(bid);
            }
        }
    }

    public class CreateDBBackup : ScheduledTask
    {
        private readonly IDBBackup _dbBackup;

        public CreateDBBackup(IDBBackup dbBackup)
        {
            this._dbBackup = dbBackup;
        }

        public override string Cron => "0 18 * * *"; // At 18:15:00 every day

        public override async Task Execute()
        {
            List<short> bids = await GetAllBIDs();

            foreach (var bid in bids)
            {
                await this._dbBackup.CreateDBBackup(bid, DbType.Business);
                await this._dbBackup.CreateDBBackup(bid, DbType.Logging);
                await this._dbBackup.CreateDBBackup(bid, DbType.Master);
            }

           
        }
    }

    public class DeleteExpiredDrafts : ScheduledTask
    {
        private readonly IDeleteExpiredDrafts _deleteExpiredDrafts;

        public DeleteExpiredDrafts(IDeleteExpiredDrafts deleteExpiredDrafts)
        {
            this._deleteExpiredDrafts = deleteExpiredDrafts;
        }

        public override string Cron => "0 * * * *"; // every hour
        public override async Task Execute()
        {
            List<short> bids = await GetAllBIDs();

            foreach (short bid in bids)
            {
                await _deleteExpiredDrafts.DeleteExpiredDrafts(bid);
            }
        }
    }

    public interface IScheduledTask
    {
        Task Execute();
        string Cron { get; }
    }

    public abstract class ScheduledTask : IScheduledTask
    {
        public static string AuthContextConnectionString { get; set; }

        public abstract string Cron { get; }

        public abstract Task Execute();

        protected async Task<List<short>> GetAllBIDs()
        {
            List<short> result = new List<short>();
            string authContextConnString = AuthContextConnectionString ?? string.Empty;

            if (String.IsNullOrWhiteSpace(authContextConnString))
                return result;

            using (var conn = new SqlConnection(authContextConnString))
            {
                var command = conn.CreateCommand();
                command.CommandText = "Select BID from [Business.Data] where IsActive = 1";
                conn.Open();
                SqlDataReader reader = await command.ExecuteReaderAsync();

                while (await reader.ReadAsync())
                {
                    if (await reader.IsDBNullAsync(0))
                        continue;

                    result.Add(reader.GetInt16(0));
                }
            }

            return result;
        }
    }
}