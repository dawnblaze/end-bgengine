﻿using Endor.AzureStorage;
using Endor.Logging.Client;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Threading.Tasks;
using Endor.EF;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace Endor.BGEngine.Core.Classes
{   
    public class SQLMaintenance : ISQLMaintenance
    {
        private readonly ITenantDataCache _cache;
        private readonly RemoteLogger _logger;

        public SQLMaintenance(ITenantDataCache cache, RemoteLogger logger)
        {
            this._cache = cache;
            this._logger = logger;
        }

        public async Task RepairUntrustedConstraint(short bid)
        {
            try
            {
                var cache = await _cache.Get(bid);
                
                var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();
                using (var ctx = new ApiContext(optionsBuilder.Options, _cache, (short)bid))
                {
                    object[] myParams = {new SqlParameter("@Repair", 1)};

                    int affectedRows = await ctx.Database.ExecuteSqlRawAsync(
                        "EXEC dbo.[Util.Constraints.CheckTrust] @Repair;",
                        parameters: myParams);
                }
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in SQLMaintenance.RepairUntrustedConstraint", ex);
            }
        }

    }
}