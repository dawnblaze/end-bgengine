﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Api.Common.Services;
using Endor.CBEL.Grammar;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing;
using Endor.RTM;
using Endor.RTM.Enums;
using Endor.RTM.Models;
using Endor.Tasks;
using Endor.Tenant;
using Hangfire;
using Hangfire.Console;
using Hangfire.Server;
using Hangfire.States;
using Irony.Parsing;
using Microsoft.EntityFrameworkCore;

namespace Endor.BGEngine.Core.Web.Classes
{
    /// <summary>
    /// Recompute CBEL For Machine
    /// </summary>
    public class RegenerateCBELForMachineTask : Endor.Tasks.IRegenerateCBELForMachine
    {
        #region Private Properties

        private readonly IRTMPushClient _rtmClient;
        private readonly RemoteLogger _logger;
        private readonly ITenantDataCache _cache;
        
        #endregion
        
        /// <summary>
        /// Order Compute Pricing
        /// </summary>
        /// <param name="rtmClient">RTM Push Client</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="cache">Tenant Data Cache</param>
        public RegenerateCBELForMachineTask(IRTMPushClient rtmClient, RemoteLogger logger, ITenantDataCache cache)
        {
            this._rtmClient = rtmClient;
            this._logger = logger;
            this._cache = cache;
        }

        /// <summary>
        /// A call to compute the Regenerate CBEL For Machine
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="orderID">Machine ID</param>
        /// <param name="context">Perform Context</param>
        /// <returns></returns>
        public async Task RegenerateCBELForMachine(short bid, int machineId,  PerformContext context)
        {
            var ctx = new ApiContext(this._cache, bid);
            var machines = ctx.MachineData.Where(m => m.BID == bid && m.ID == machineId);
            if (machines.Count()==0)
            {
                Exception ex = new Exception("Could not find machine with Bid: {bid} and Id: {machineId}");
                await _logger.Error(bid, "Could not find machine with Bid: {bid} and Id: {machineId}", ex);
                throw ex;
            }
            try
            {
                var model = machines.First();
                if (model.MachineTemplate == null && model.MachineTemplateID.HasValue)
                    model.MachineTemplate = ctx.AssemblyData.WherePrimary(bid, model.MachineTemplateID.Value).Include(w => w.Tables).Include(x => x.Variables).ThenInclude(y => y.Formulas).AsNoTracking().FirstOrDefault();

                if (model.MachineTemplate == null)
                {
                    throw new InvalidOperationException($"Machine (ID:{model.ID}) does not have Machine Template (ID:{model.MachineTemplateID}) so RegenerateCBELForMachine cannot be completed");
                }

                //Create CBELAssemblyGenerator
                CBELMachineGenerator assemblyGenerator = CBELMachineGenerator.Create(ctx, model);
                //Save off files    
                CBEL.Common.AssemblyCompilationResponse compilationResult = await assemblyGenerator.Save(this._cache);
            }
            catch (Exception ex)
            {
                await _logger.Error(bid, "Error in RegenerateCBELForMachine.RegenerateCBELForMachine", ex);
                throw;
            }
        }
    }
}
