﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.BGEngine.Core;
using Newtonsoft.Json;

namespace Endor.BGEngine.Core.Web.Classes
{
    public class DocumentReportTemplateDTO
    {
        /// <summary>
        /// The Name of this Template.
        /// <para>
        /// The template name must be unique within all Report Types (both Custom and Standard).
        /// </para>
        /// </summary>
        [JsonRequired]
        public string TemplateName { get; set; }
        /// <summary>
        /// If there is Template File Data
        /// </summary>
        public bool HasTemplateFile { get; set; }
        /// <summary>
        /// The ClassTypeID this Template applies to.
        /// </summary>
        public int AppliesToClassTypeID { get; set; }
        /// <summary>
        /// Enum indicating the type of DocumentReportType.
        /// </summary>
        public byte DocumentReportType { get; set; }
        /// <summary>
        /// Flag indicating if this report is a Standard Template (true) stored in the /aim{aim}/ path or a Custom Template (false) stored in the /bid{bid}/ path.
        /// </summary>
        public bool IsStandardTemplate { get; set; }
        /// <summary>
        /// Tooltip text
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Tooltip { get; set; }
        /// <summary>
        /// The OptionsDefinitions for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OptionsDefinition> OptionsDefinitions { get; set; }
        /// <summary>
        /// The Date and Time the Template was last updated in DM.
        /// </summary>
        public DateTime ModifiedDT { get; set; }
        /// <summary>
        /// Base64 Encoded Template File Data
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TemplateFileData { get; set; }
    }
}
