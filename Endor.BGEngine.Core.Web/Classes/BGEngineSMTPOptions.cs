﻿using Endor.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Configuration;

namespace Endor.BGEngine.Core.Classes
{
    public class BGEngineSMTPOptions : ISMTPOptions
    {

        public BGEngineSMTPOptions(IOptions<SMTPOptions> smtpOptions)
        {
            this.Host = smtpOptions.Value.Host;

            this.Port = smtpOptions.Value.Port;

            this.UserName = smtpOptions.Value.UserName;
            this.Password = smtpOptions.Value.Password;
            this.MailFromAddress = smtpOptions.Value.MailFromAddress;
            
            this.IsValid = !String.IsNullOrWhiteSpace(this.Host) &&
                !String.IsNullOrWhiteSpace(this.UserName) &&
                !String.IsNullOrWhiteSpace(this.Password) &&
                !String.IsNullOrWhiteSpace(this.MailFromAddress) &&
                this.Port != default(int);
        }

        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string MailFromAddress { get; set; }
        public bool IsValid { get; internal set; }
    }
}