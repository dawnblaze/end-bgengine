﻿using Endor.Tenant;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace Endor.BGEngine.Core.Classes
{
    public class CacheOptions : IEnvironmentOptions, IOptions<MemoryCacheOptions>
    {
        public string AuthOrigin { get; set; }

        public string TenantSecret { get; set; }

        public MemoryCacheOptions Value => new MemoryCacheOptions();
    }
}