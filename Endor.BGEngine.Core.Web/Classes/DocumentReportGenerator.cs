﻿using Endor.AzureStorage;
using Endor.DocumentStorage.Models;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Endor.BGEngine.Core;
using System.Net.Http.Headers;
using Endor.BGEngine.Core.Classes;
using System.Linq;

namespace Endor.BGEngine.Core.Web.Classes
{
    public class DocumentReportGenerator : IDocumentReportGenerator
    {
        private readonly ITenantDataCache _cache;
        private readonly CacheOptions _options;
        private readonly RemoteLogger _logger;
        
        public DocumentReportGenerator(ITenantDataCache tenantData, CacheOptions options, RemoteLogger logger)
        {
            this._cache = tenantData;
            this._options = options;
            this._logger = logger;
        }
        public async Task GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            try
            {
                var tenantData = await this._cache.Get(bid);

                using (var client = new HttpClient())
                {
                    var queryString = BuildQueryString(DataID, CustomFields, OrderStatusFilter, OptionsValue);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Internal", $"{this._options.TenantSecret}:{bid}:{aid}");
                    string[] ext = DMFilePath.Split(".");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(String.Format("application/{0}",ext[ext.Length-1]).ToLower()));
                    var response = await client.GetAsync($"{tenantData.ReportingURL}Api/DocumentReport/generate/{documentReportType}/menu/{MenuID}{queryString}");

                    if (response.IsSuccessStatusCode)
                    {
                        StoreGeneratedFile(response, tenantData, bid, documentReportType, DMFilePath);
                    }
                    else
                    {
                        await _logger.Information(bid, $"GenerateDocumentReportByMenuID returns {response.StatusCode}");
                    }
                }
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"GenerateDocumentReportByMenuID returns {e.Message}", e);
            }
        }

        public async Task GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            try
            {
                var tenantData = await this._cache.Get(bid);
                using (var client = new HttpClient())
                {
                    var queryString = BuildQueryString(DataID, CustomFields, OrderStatusFilter, OptionsValue);

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Internal", $"{this._options.TenantSecret}:{bid}:{aid}");
                    string[] ext = DMFilePath.Split(".");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(String.Format("application/{0}", ext[ext.Length - 1]).ToLower()));
                    var response = await client.GetAsync($"{tenantData.ReportingURL}Api/DocumentReport/generate/{documentReportType}/template/{templateName}{queryString}");

                    if (response.IsSuccessStatusCode)
                    {
                        StoreGeneratedFile(response, tenantData, bid, documentReportType, DMFilePath);
                    }
                    else
                    {
                        await _logger.Information(bid, $"GenerateDocumentReportByTemplateName returns {response.StatusCode}");
                    }
                }
            }
            catch (Exception e)
            {
                await _logger.Error(bid, $"Error when generation report {e.Message}", e);
            }
        }

        private static int AppliesToClassTypeFromDocumentReportType(DocumentReportType documentReportType)
        {

            switch (documentReportType)
            {
                case DocumentReportType.Estimate:
                    return (int)ClassType.Estimate;
                case DocumentReportType.WorkOrder:
                case DocumentReportType.Invoice:
                case DocumentReportType.PackingSlip:
                    return (int)ClassType.Order;
                case DocumentReportType.Proposal:
                    return (int)ClassType.Opportunity;
                case DocumentReportType.Statement:
                    return (int)ClassType.Company;
                case DocumentReportType.PurchaseOrder:
                    return 10100; //ReportingConstants.ClassType.PurchaseOrder;
                default:
                    return (int)ClassType.Order;
            }
        }

        private static string BuildQueryString(int[] dataID, int[] customFields, string orderStatusFilter, string optionsValue)
        {
            var queryString = "";
            if (dataID.Length > 0)
            {
                string param = "";
                foreach (var id in dataID)
                {
                    param += String.Format("{0}dataID={1}", param.Length == 0 ? "" : "&", id.ToString());
                }
                queryString += String.Format("{0}{1}", queryString.Length == 0 ? "?" : "&", param);
            }
            if (customFields.Length > 0)
            {
                string param = "";
                foreach (var cf in customFields)
                {
                    param += String.Format("{0}customFields={1}", param.Length == 0 ? "" : "&", cf.ToString());
                }
                queryString += String.Format("{0}{1}", queryString.Length == 0 ? "?" : "&", param);
            }
            
            if (orderStatusFilter != null) queryString += queryString.Length == 0 ? "?orderStatusFilter=" + orderStatusFilter : "&orderStatusFilter=" + orderStatusFilter;
            if (optionsValue != null) queryString += queryString.Length == 0 ? "?optionsValue=" + optionsValue : "&optionsValue=" + optionsValue;
            return queryString;
        }

        private async void StoreGeneratedFile(HttpResponseMessage response, TenantData tenantData, short bid, byte documentReportType, string DMFilePath)
        {
            string[] fileInfo = DMFilePath.Split("/");
            string fileName = fileInfo[fileInfo.Count() - 1];
            var values = Enum.GetValues(typeof(Bucket)).Cast<Bucket>();
            var bucket = values.Where(x => x.ToString().ToLower() == fileInfo[0].ToLower()).FirstOrDefault();

            fileInfo = fileInfo.Skip(1).ToArray();
            fileInfo = fileInfo.Skip(1).ToArray();
            fileInfo = fileInfo.Take(fileInfo.Count()-1).ToArray();
            string filePath = String.Join("/", fileInfo);

            Stream ms = new MemoryStream(await response.Content.ReadAsByteArrayAsync());
            EntityStorageClient azureStorage = new EntityStorageClient(tenantData.StorageConnectionString, bid);
            var contentType = response.Content.Headers.ContentType.MediaType;
            var dmid = new Endor.DocumentStorage.Models.DMID { ctid = AppliesToClassTypeFromDocumentReportType((DocumentReportType)documentReportType), classFolder = filePath };

            await azureStorage.AddFile(
                stream: ms,
                storageBin: StorageBin.Permanent,
                bucket: bucket,
                id: dmid,
                fileName: fileName,
                contentType: contentType,
                createdByID: -1,
                createdByCTID: AppliesToClassTypeFromDocumentReportType((DocumentReportType)documentReportType)
            );
            await response.Content.ReadAsStringAsync();
        }
    }
}
