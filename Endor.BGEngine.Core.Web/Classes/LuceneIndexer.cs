﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Endor.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Endor.BGEngine.Core.Classes
{
    public class LuceneIndexer : IIndexer
    {
        private readonly ITenantDataCache _cache;
        private readonly CacheOptions _options;
        
        public LuceneIndexer(ITenantDataCache cache, CacheOptions options)
        {
            this._cache = cache;
            this._options = options;
        }

        #region iindexer
        public async Task Index(short bid, int ctid)
        {
            var response = await GetSearchClient(bid, _options.TenantSecret).GetAsync($"{await BaseUrl(bid)}/{bid}/{ctid}");
            if (response.IsSuccessStatusCode)
            {
                return;
            }

            throw new HttpRequestException(response.ReasonPhrase);
        }

        public async Task Index(short bid, int ctid, ICollection<int> ids)
        {
            var response = await GetSearchClient(bid, _options.TenantSecret).GetAsync($"{await BaseUrl(bid)}/{bid}/{ctid}/multiple/{string.Join(",",ids)}");
            if (response.IsSuccessStatusCode)
            {
                return;
            }

            throw new HttpRequestException(response.ReasonPhrase);
        }

        public async Task Index(short bid, int ctid, int id)
        {
            var response = await GetSearchClient(bid, _options.TenantSecret).GetAsync($"{await BaseUrl(bid)}/{bid}/{ctid}/{id}");
            if (response.IsSuccessStatusCode)
            {
                return;
            }

            throw new HttpRequestException(response.ReasonPhrase);
        }

        #endregion

        private async Task<string> BaseUrl(short bid)
        {
            TenantData tenantData = await _cache.Get(bid);
            string searchServerUrl = tenantData.SearchURL;
            return $"{searchServerUrl}api/index";
        }

        private HttpClient GetSearchClient(short bid, string key)
        {
            var _searchClient = new HttpClient();
            _searchClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Internal", $"{key}:{bid}");
            _searchClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _searchClient.Timeout = new TimeSpan(0, 15, 0);
            return _searchClient;
        }
    }

}