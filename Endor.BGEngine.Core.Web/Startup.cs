using Endor.Api.Common.Classes;
using Endor.Authentication;
using Endor.BGEngine.Core.Classes;
using Endor.BGEngine.Core.Web.Classes;
using Endor.Configuration;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Hangfire;
using Hangfire.Console;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.OpenApi.Models;

namespace Endor.BGEngine.Core.Web
{
    public abstract class BaseStartup
    {
        private const string HangfireSQLConnectionStringSettingName = "HangfireSQL";
        private readonly IWebHostEnvironment _env;

        private ServiceProvider _provider { get; set; }
        private BackgroundJobServer _backgroundJobServer;

        /// <summary>
        /// Configures External Endor Services
        /// </summary>
        /// <param name="services"></param>
        public virtual void ConfigureExternalEndorServices(IServiceCollection services)
        {
            services.AddSingleton<ITenantDataCache, NetworkTenantDataCache>();
            services.AddSingleton<ITaskQueuer>(new HttpTaskQueuer(Configuration["Endor:TasksAPIURL"], Configuration["Endor:TenantSecret"]));
            services.AddSingleton<RemoteLogger>(x => {
                ITenantDataCache cache = x.GetService<ITenantDataCache>();
                RemoteLogger logger = new RemoteLogger(cache);
                logger.TenantSecret = Configuration["Endor:TenantSecret"];
                return logger;
            });

            services.AddSingleton<ISQLMaintenance, SQLMaintenance>();
            services.AddSingleton<IDocCleaner, DocCleaner>();
            services.AddSingleton<IDeleteExpiredDrafts, DeleteDraftsTask>();

            services.AddSingleton<IIndexer, LuceneIndexer>();
            services.AddSingleton<IAssemblyCleaner, AssemblyCleaner>();

            services.AddSingleton<ICheckOrderStatus, CheckOrderStatusTask>();
            services.AddSingleton<IRecomputeGL, RecomputeGLTask>();
            services.AddSingleton<IOrderPricingCompute, OrderComputePricing>();
            services.AddSingleton<IRegenerateCBELForMachine, RegenerateCBELForMachineTask>();
            services.AddSingleton<IDBBackup, DBBackupTask>();
            services.AddSingleton<IDocumentReportGenerator, DocumentReportGenerator>();
            services.AddSingleton<IReconciliation, ReconciliationTask>();

            services.AddSingleton<ITester, Tester>();

            services.AddSingleton<IMemoryCache, MemoryCache>();
            services.AddSingleton<IRTMPushClient, TenantPushClient>();
            services.AddSingleton<ISMTPOptions, BGEngineSMTPOptions>();
            services.AddSingleton<IMigrationHelper, MigrationHelper>();
        }

        public BaseStartup(IWebHostEnvironment env)
        {
            this._env = env;
            IConfigurationBuilder builder = GetConfigurationBuilder(env);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
                
            }

            Configuration = builder.Build();

            if (env.IsDevelopment() && Configuration["Endor:StorageConnectionString"] != null && Configuration["Endor:StorageConnectionString"].Contains("local"))
            {
                Task.Run(AzureStorage.EntityStorageClient.ConfigureLocalDevelopmentCORS).ConfigureAwait(false);
            }

        }

        private IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            return new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CacheOptions>(Configuration.GetSection("Endor"));
            services.AddMemoryCache();
            CacheOptions cacheOptions = Configuration.GetSection("Endor").Get<CacheOptions>();
            services.AddSingleton<Tenant.IEnvironmentOptions>(cacheOptions);
            services.AddSingleton<CacheOptions>(cacheOptions);

            ConfigureExternalEndorServices(services);

            services.AddAuthentication().AddInternal((options) => { options.TenantSecretKey = cacheOptions.TenantSecret; });
            services.AddAuthorization();

            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            });

            services.AddHangfire(options =>
            {
                options.UseSqlServerStorage(Configuration.GetConnectionString(HangfireSQLConnectionStringSettingName), new Hangfire.SqlServer.SqlServerStorageOptions()
                {
                    QueuePollInterval = TimeSpan.FromSeconds(1),
                    JobExpirationCheckInterval = TimeSpan.FromMinutes(3),
                    DashboardJobListLimit = 5000,
                });
                options.UseConsole();
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Endor BGEngine",
                    Version = "v1"
                });
#pragma warning disable CS0618
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteActions();
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.OperationFilter<SwaggerSecurityRightsDocumentFilter>();
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".XML";
                string commentsFile = System.IO.Path.Combine(baseDirectory, commentsFileName);

                if (System.IO.File.Exists(commentsFile))
                {
                    c.IncludeXmlComments(commentsFile);
                }

                c.SchemaFilter<SwaggerAddMissingEnums>();
            });
#pragma warning disable ASP0000
            _provider = services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            RemoteLoggingExtensions.ConfigureSystemRemoteLogging(Configuration, loggerFactory, appLifetime);

            app.UseStaticFiles();

            app.UseSwagger(o =>
            {
                o.PreSerializeFilters.Add((swagger, httpReq) =>
                {
                    swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}" } };
                });
            })
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Endor BGEngine v1");
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = "swagger";
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[]
                {
                    new HangfireDashboardAuthorizationFilter(Configuration)
                }
            });
            app.UseHangfireServer();
            app.UseHttpsRedirection();
#pragma warning disable MVC1005
            app.UseMvc();
            app.UseRouting();

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute() { OnAttemptsExceeded = AttemptsExceededAction.Delete });

            _backgroundJobServer = new BackgroundJobServer(new BackgroundJobServerOptions()
            {
                Queues = new[] { "high", "medium", "default", "low" },
                WorkerCount = Environment.ProcessorCount * 2
            });

            var docCleaner = _provider.GetService<IDocCleaner>();
            var deleteExpiredDrafts = _provider.GetService<IDeleteExpiredDrafts>();
            var repairUntrustedConstraint = _provider.GetService<ISQLMaintenance>();
            var dbBackup = _provider.GetService<IDBBackup>();



            EmptyTemp emptyTemp = new EmptyTemp(docCleaner);
            EmptyTrash emptyTrash = new EmptyTrash(docCleaner);
            DeleteExpiredDrafts _deleteExpiredDrafts = new DeleteExpiredDrafts(deleteExpiredDrafts);
            RepairUntrustedConstraint _repairUntrustedConstraint = new RepairUntrustedConstraint(repairUntrustedConstraint);
            CreateDBBackup createDbBackup = new CreateDBBackup(dbBackup);


            Hangfire.RecurringJob.AddOrUpdate(() => emptyTemp.Execute(), emptyTemp.Cron);
            Hangfire.RecurringJob.AddOrUpdate(() => emptyTrash.Execute(), emptyTrash.Cron);
            Hangfire.RecurringJob.AddOrUpdate(() => _deleteExpiredDrafts.Execute(), _deleteExpiredDrafts.Cron);
            Hangfire.RecurringJob.AddOrUpdate(() => _repairUntrustedConstraint.Execute(), _repairUntrustedConstraint.Cron);
            Hangfire.RecurringJob.AddOrUpdate(() => createDbBackup.Execute(), createDbBackup.Cron);
        }
    }

    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup : BaseStartup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env) : base(env)
        {
        }

    }
}

