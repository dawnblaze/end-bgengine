﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Endor.BGEngine.Core.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SetConfiguration("Endor.BGEngine");
            //CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();


        public static void SetConfiguration(string title)
        {
            Console.Title = title;
            IConfigurationBuilder hostingBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables();

            var host = new WebHostBuilder();
            var environment = host.GetSetting("environment");
            hostingBuilder.SetBasePath(Directory.GetCurrentDirectory());
            hostingBuilder.AddJsonFile("appsettings.json");
            hostingBuilder.AddJsonFile($"appsettings.{environment}.json", optional: true);

            if (environment == "Development")
            {
                hostingBuilder.AddUserSecrets<Startup>();
            }

            var envConfiguration = hostingBuilder.Build();

            host.UseKestrel(options =>
            {
                if (environment == "Development")
                {
                    AddHttpsWithCert(options, envConfiguration);
                }
            })
                .UseIISIntegration()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .Build()
                .Run();

        }

        public static void AddHttpsWithCert(KestrelServerOptions options, IConfigurationRoot envConfiguration)
        {
            Uri hostUri = new Uri(envConfiguration["ASPNETCORE_URLS"]);
            if (Environment.OSVersion.ToString().Contains("Unix"))
            {
                string wildCardCertPath = "/usr/local/share/ca-certificates/wildcard.localcyriousdevelopment.com.v3.GOES_IN_PERSONAL.pfx";
                string wildCardCertPassword = envConfiguration["wildCardCertPassword"];
                options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(wildCardCertPath, wildCardCertPassword)));
            }
            else
            {
                X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection certs = store.Certificates.Find(X509FindType.FindByThumbprint, envConfiguration["Endor:certThumbprint"], true);
                if (certs != null && certs.Count > 0)
                {
                    X509Certificate2 cert = certs[0];

                    options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(cert))); //options.UseHttps(cert);
                }
            }
        }
    }
}
